#if (UNITY_EDITOR)
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreateMKButton : MonoBehaviour
{
    [MenuItem("GameObject/UI/Mikuni Button")]
    static void CreateButton()
    {
        GameObject prefabButton =
            (GameObject) PrefabUtility.InstantiatePrefab(
                AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/UI/MKButton.prefab"));
        GameObject parent = Selection.activeGameObject;
        prefabButton.transform.SetParent(parent.transform);
    }
}
#endif