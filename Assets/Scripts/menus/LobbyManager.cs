﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using ExitGames.Client.Photon;
using menus.Lobby;
using network;
using Photon.Pun;
using Photon.Realtime;
using sound;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using ustensils;

namespace menus
{
    public class LobbyManager : MonoBehaviourPunCallbacks
    {
        public int fallbackScene = 0;
        private PhotonView _view;
        public GameObject startGame;
        private readonly List<(Player, bool, long)> _readiness = new List<(Player, bool, long)>();
        public GameObject[] PogYouNametags;
        public GameObject[] WeaponButtons;
        public GameObject ustensilHolder;
        public GameObject gameId;
        public PlayerListPopup popup;
        public RoomConfigPopup configPopup;

        [FormerlySerializedAs("groupName")] public Text roomNameText;
        [FormerlySerializedAs("roomCounter")] public Text playerCounterText;

        private int _lastWeaponId = 0;
        private Utensil[] _utensils;
        private int[] utensilsStrengths;
        private int[] utensilsSpeeds;
        private Hashtable _roomCustomProperties;

        public override void OnEnable()
        {
            base.OnEnable();
            
            GameObject[] utensils = PlayerInfo.PInfo.allUstencils;
            int len = utensils.Length;
            utensilsStrengths = new int[len];
            utensilsSpeeds = new int[len];
            _utensils = new Utensil[len];

            List<(int, float)> strenghts = new List<(int, float)>(len);
            List<(int, float)> speeds = new List<(int, float)>(len);

            for (var i = 0; i < len; i++)
            {
                GameObject gm = utensils[i];
                Utensil utensil = gm.GetComponent<Utensil>();
                _utensils[i] = utensil;
                strenghts.Add((i, utensil.strength));
                speeds.Add((i, utensil.speed));
            }

            strenghts.Sort((a, b) => a.Item2.CompareTo(b.Item2));
            speeds.Sort((a, b) => b.Item2.CompareTo(a.Item2));
            for (var i = 0; i < len; i++)
            {
                utensilsStrengths[strenghts[i].Item1] = i + 1;
                utensilsSpeeds[speeds[i].Item1] = i + 1;
            }

            gameId.GetComponent<TextMeshProUGUI>().text += PhotonNetwork.CurrentRoom.Name;
        }

        public override void OnDisable()
        {
            base.OnDisable();
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        private void Start()
        {
            AudioManager.CurrentManager.StartBgMusic();
            _view = GetComponent<PhotonView>();
            foreach (var pair in PhotonNetwork.CurrentRoom.Players)
            {
                long joinDate;
                if (pair.Value.CustomProperties?["JoinDate"] != null)
                {
                    joinDate = (long) pair.Value.CustomProperties["JoinDate"];
                }
                else
                {
                    joinDate = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                }
                _readiness.Add((pair.Value, false, joinDate));
            }

            popup.allPlayersReady = false;
            roomNameText.text = (string) PhotonNetwork.CurrentRoom.CustomProperties["Name"];
            _roomCustomProperties = new Hashtable();
            SetRoomName(PhotonNetwork.MasterClient.NickName + "'s room");
            SetRoomMikunis(40);
            SetRoomTime(180);
            SetRoomMasterName(PhotonNetwork.LocalPlayer.NickName);
            Render();
        }

        private void LateUpdate()
        {
            if (PhotonNetwork.CurrentRoom?.CustomProperties != null)
            {
                playerCounterText.text = _readiness.Count + "/4";
                roomNameText.text = (string) PhotonNetwork.CurrentRoom.CustomProperties["Name"];
            }
            if (_lastWeaponId != PlayerInfo.PInfo.selectedUstencil)
            {
                _lastWeaponId = PlayerInfo.PInfo.selectedUstencil;
                Transform tr = ustensilHolder.transform;
                for (int j = 0; j < tr.childCount; j++)
                {
                    Transform child = tr.GetChild(j);
                    child.parent = null;
                    Destroy(child.gameObject);
                }

                GameObject utensil = Instantiate(PlayerInfo.PInfo.allUstencils[_lastWeaponId],
                    tr.position, tr.rotation, tr);
                if (utensil.GetComponent<Utensil>() is GrapplingForkUtensil gf)
                {
                    gf.HideUI();
                }

                utensil.transform.parent = tr;
            }
        }

        private void FixedUpdate()
        {
            int i = 0;
            foreach (GameObject button in WeaponButtons)
            {
                button.GetComponent<WeaponButton>().selected = i == PlayerInfo.PInfo.selectedUstencil;
                if (i == PlayerInfo.PInfo.selectedUstencil)
                {
                    Utensil weapon = _utensils[i];
                    WeaponStatsDisplayer.Name = weapon.LocalizedName;
                    WeaponStatsDisplayer.speed = utensilsSpeeds[i];
                    WeaponStatsDisplayer.capacity = Mathf.RoundToInt(weapon.capacity);
                    WeaponStatsDisplayer.strength = utensilsStrengths[i];
                    WeaponStatsDisplayer.range = Mathf.RoundToInt(weapon.range);
                }

                i++;
            }

            if (PhotonNetwork.CurrentRoom?.CustomProperties == null || !PhotonNetwork.IsConnectedAndReady) return;
            if (String.IsNullOrWhiteSpace(PhotonNetwork.CurrentRoom.CustomProperties["Name"] as string))
            {
                SetRoomName(PhotonNetwork.MasterClient.NickName + "'s room");
            }
            SetRoomMasterName(PhotonNetwork.MasterClient.NickName);
        }

        public void SetWeapon(int id)
        {
            PlayerInfo.PInfo.SetSelectedUstencil(id);
        }

        public void StartGame()
        {
            if (PhotonNetwork.CurrentRoom == null) return;
            PhotonRoom.CurrentRoom.StartGame();
        }

        public void Ready()
        {
            Player localPlayer = PhotonNetwork.LocalPlayer;
            int i = FindPlayer(localPlayer.ActorNumber);
            if (i == -1) return;
            _view.RPC("RPC_UpdateReadyState", RpcTarget.AllBuffered,
                localPlayer.ActorNumber, true);
        }

        public void Unready()
        {
            Player localPlayer = PhotonNetwork.LocalPlayer;
            int i = FindPlayer(localPlayer.ActorNumber);
            if (i == -1) return;
            _view.RPC("RPC_UpdateReadyState", RpcTarget.AllBuffered,
                localPlayer.ActorNumber, false);
        }

        public void Leave()
        {
            if(PhotonNetwork.CurrentRoom != null)
                PhotonRoom.CurrentRoom.Leave();
        }

        public void Render()
        {
            int currentActor = PhotonNetwork.LocalPlayer.ActorNumber;
            int i = 0;
            _readiness.Sort((a, b) => a.Item3.CompareTo(b.Item3));
            foreach (var pair in _readiness)
            {
                GameObject pogYou = PogYouNametags[i];
                popup.gridElements[i].playerName = pair.Item1.NickName ?? "...";
                popup.gridElements[i].ready = pair.Item2;
                pogYou.SetActive(pair.Item1.ActorNumber == currentActor);
                i++;
            }

            for (; i < 4; i++)
            { 
                popup.gridElements[i].playerName = "...";
                popup.gridElements[i].ready = false;
                PogYouNametags[i].SetActive(false);
            }

            if (configPopup != null)
            {
                configPopup.Players = _readiness.ConvertAll(tuple => tuple.Item1);
            }
            popup.allPlayersReady = _readiness.All(readiness => readiness.Item2);
        }

        private int FindPlayer(int actor)
        {
            int l = _readiness.Count, i = 0;
            while (i < l && _readiness[i].Item1.ActorNumber != actor) i++;

            if (i == l) return -1;
            return i;
        }

        public void SetRoomName(string name)
        {
            if (!PhotonNetwork.IsMasterClient) return;
            _roomCustomProperties["Name"] = name;
            PhotonNetwork.CurrentRoom.SetCustomProperties(_roomCustomProperties);
        }

        public void SetRoomMikunis(int mikunis)
        {
            if (!PhotonNetwork.IsMasterClient) return;
            _roomCustomProperties["Mikunis"] = mikunis;
            PhotonNetwork.CurrentRoom.SetCustomProperties(_roomCustomProperties);
        }

        public void SetRoomTime(int time)
        {
            if (!PhotonNetwork.IsMasterClient) return;
            _roomCustomProperties["Time"] = time;
            PhotonNetwork.CurrentRoom.SetCustomProperties(_roomCustomProperties);
        }
        
        public void SetPasswordCheck(string hash)
        {
            if (!PhotonNetwork.IsMasterClient) return;
            _roomCustomProperties["Password"] = hash;
            PhotonNetwork.CurrentRoom.SetCustomProperties(_roomCustomProperties);
        }

        public void SetRoomMasterName(string username)
        {
            if (!PhotonNetwork.IsMasterClient) return;
            _roomCustomProperties["Master"] = username;
            PhotonNetwork.CurrentRoom.SetCustomProperties(_roomCustomProperties);
        }

        public void OnKickPlayerButtonPressed(int index)
        {
            if (!PhotonNetwork.IsMasterClient || index >= _readiness.Count || index < 0) return;
            _view.RPC("RPC_NotifyPlayerKicked", RpcTarget.All, _readiness[index].Item1.ActorNumber);
        }
        
        public void OnMikuniQuotaUpdated()
        {
            if (!PhotonNetwork.IsMasterClient) return;
            int val = Mathf.RoundToInt(configPopup.editableMikuniQuota.EditModeGameObject
                .GetComponentInChildren<Slider>().value);
            SetRoomMikunis(val);
        }

        public void OnPasswordUpdated(InputField ip)
        {
            if (!PhotonNetwork.IsMasterClient) return;
            string val = ip.text;
            SetPasswordCheck(string.IsNullOrWhiteSpace(val) ? "" : ComputeSha256Hash(val));
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            int i = FindPlayer(otherPlayer.ActorNumber);
            if (i == -1) return;
            _readiness.RemoveAt(i);
            Render();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            _readiness.Add((newPlayer, false, DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond));
            Render();
        }

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            // maybe not update that much: a player may be able to spam this event and ddos other players (yikes)
            Render();
        }

        public override void OnLeftRoom()
        {
            SceneManager.LoadSceneAsync(fallbackScene);
        }
        
        [PunRPC]
        public void RPC_UpdateReadyState(int actor, bool ready)
        {
            int i = FindPlayer(actor);
            if (i != -1)
            {
                var (pl, _, tp) = _readiness[i];
                _readiness[i] = (pl, ready, tp);
            }

            Render();
        }

        [PunRPC]
        public void RPC_NotifyPlayerKicked(int actor)
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber == actor)
            {
                Leave();
            }
        }

        static string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();
            }
        }
    }
}