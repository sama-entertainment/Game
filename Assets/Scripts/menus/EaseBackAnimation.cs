using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class EaseBackAnimation : MonoBehaviour
{

    public static bool AllFinished;
    
    [FormerlySerializedAs("enabled")] public bool enable;
    [Range(0.001f, 1f)] public float speed;

    private float _animProgress;
    private float _updatedProgress;
    private const float C1 = 1.70158f;
    private const float C3 = 2.70158f;
    private const int framerateDiv = 1;
    private int _timer;

    // Start is called before the first frame update
    void Start()
    {
        _animProgress = enable ? 1f : 0;
        _timer = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_timer >= framerateDiv) _timer = 0;
        if (_timer == 0)
        {
            if (enable)
            {
                if (_animProgress < 1f)
                {
                    // _animProgress += 1f - Mathf.Exp(-5f * speed);
                    _animProgress += speed / 5;
                    if (AllFinished)
                    {
                        AllFinished = false;
                    }
                }
                else
                {
                    if (!AllFinished)
                    {
                        AllFinished = true;
                    }
                }
            }
            else
            {
                if (_animProgress > 0)
                {
                    // _animProgress -= 1f - Mathf.Exp(-5f * speed);
                    _animProgress -= speed / 5;
                    if (AllFinished)
                    {
                        AllFinished = false;
                    }
                }
                else
                {
                    if (!AllFinished)
                    {
                        AllFinished = true;
                    }
                }
            }
        }

        _timer++;

        _animProgress = Mathf.Clamp01(_animProgress);

        _updatedProgress = EaseOutBack(_animProgress);
    }

    void Update()
    {
        transform.localScale = new Vector3(_updatedProgress, _updatedProgress, _updatedProgress);
    }

    private float EaseOutBack(float input) => 1 + C3 * Mathf.Pow(input - 1, 3) + C1 * Mathf.Pow(input - 1, 2);
}