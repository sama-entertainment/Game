using System;
using System.Collections;
using System.Collections.Generic;
using menus.RoomBrowser;
using network;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class JoinNamedRoomPopup : MonoBehaviourPunCallbacks
{
    public Text statusText;
    [FormerlySerializedAs("confirmButton")] public GameObject joinButton;
    public GameObject popupObject;
    public InputField codeInput;
    public RoomListManager roomListManager;
    
    private bool _enabled;
    private CanvasGroup _canvasGroup;
    private EaseBackAnimation _popupAnimation;
    public CanvasGroup dimBG;
    public RoomListing roomListing;
    
    public bool Opened => _enabled;

    private bool _failed;
    private bool _connecting;
    
    private Button _joinButtonComponent;

    public void Open()
    {
        _failed = false;
        _enabled = true;
        _canvasGroup.interactable = true;
    }

    // Start is called before the first frame update
    void Start()
    { 
        _enabled = false;
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.interactable = true;
        _popupAnimation = popupObject.GetComponent<EaseBackAnimation>();
        _joinButtonComponent = joinButton.transform.GetChild(0).gameObject.GetComponent<Button>();
    }

    private void Update()
    {
        _popupAnimation.enable = _enabled;
        statusText.gameObject.SetActive(_failed);
        _joinButtonComponent.interactable = !_connecting;
    }

    public void JoinNamedRoom()
    {
        string roomName = codeInput.text.ToUpperInvariant().Replace("mk:room:", String.Empty);
        if (!roomListManager.RoomList.ContainsKey("mk:room:" + roomName))
        {
            _failed = true;
            statusText.text = "La partie n'existe pas.";
            return;
        }

        RoomInfo roomInfo = roomListManager.RoomList["mk:room:" + roomName];

        if (!String.IsNullOrWhiteSpace((string) roomInfo.CustomProperties["Password"]))
        {
            Close();
            roomListing.passwordPopup.Open("mk:room:" + roomName);
            return;
        }
        _failed = false;
        _connecting = true;
        PhotonNetwork.JoinRoom("mk:room:" + roomName);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        _failed = true;
        _connecting = false;
        statusText.text = "La connection a échouée";
    }

    public override void OnJoinedRoom()
    {
        _connecting = false;
        Close();
    }

    void FixedUpdate()
    { 
        if (_enabled)
        {
            if (dimBG.alpha < 1f)
            {
                dimBG.alpha += 0.04f;
            }
        }
        else
        {
            if (dimBG.alpha > 0)
            {
                dimBG.alpha -= 0.04f;
            }
        }
        
    }


    public void Close()
    {
        _enabled = false;
        _canvasGroup.interactable = false;
    }
}
