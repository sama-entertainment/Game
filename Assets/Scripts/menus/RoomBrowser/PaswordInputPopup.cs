using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using JetBrains.Annotations;
using menus.RoomBrowser;
using network;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PaswordInputPopup : MonoBehaviourPunCallbacks
{
    public Text statusText;
    public GameObject joinButton;
    public GameObject popupObject;
    public InputField codeInput;
    public RoomListManager roomListManager;
    public RoomListing roomListing;

    private bool _enabled;
    private CanvasGroup _canvasGroup;
    private EaseBackAnimation _popupAnimation;
    public CanvasGroup dimBG;

    public bool Opened => _enabled;

    private bool _failed;
    private bool _connecting;
    private string _roomId;

    [CanBeNull]
    private RoomInfo CurrentRoom
    {
        get
        {
            try
            {
                return roomListManager.RoomList[_roomId];
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    private Button _joinButtonComponent;

    public void Open(string roomId = null)
    {
        _roomId = roomId ?? roomListing.SelectedRoom?.Name ?? "";
        _failed = false;
        _enabled = true;
        _canvasGroup.interactable = true;
        codeInput.text = "";
    }

    // Start is called before the first frame update
    void Start()
    {
        _enabled = false;
        _canvasGroup = GetComponent<CanvasGroup>();
        _canvasGroup.interactable = true;
        _popupAnimation = popupObject.GetComponent<EaseBackAnimation>();
        _joinButtonComponent = joinButton.transform.GetChild(0).gameObject.GetComponent<Button>();
    }

    private void Update()
    {
        _popupAnimation.enable = _enabled;
        statusText.gameObject.SetActive(_failed);
        _joinButtonComponent.interactable = !_connecting;
    }

    public void JoinNamedRoom()
    {
        if (CurrentRoom == null)
        {
            _failed = true;
            statusText.text = "La partie n'existe pas.";
            return;
        }
        
        string plainPassword = codeInput.text;
        string hashedPassword = ComputeSha256Hash(plainPassword);


        if (hashedPassword != (string) CurrentRoom.CustomProperties["Password"])
        {
            _failed = true;
            statusText.text = "Mot de passe incorrect.";
            return;
        }

        _failed = false;
        _connecting = true;
        PhotonNetwork.JoinRoom(CurrentRoom.Name);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        _failed = true;
        _connecting = false;
        statusText.text = "La connection a échouée";
    }

    public override void OnJoinedRoom()
    {
        _connecting = false;
        Close();
    }

    void FixedUpdate()
    {
        if (_enabled)
        {
            if (dimBG.alpha < 1f)
            {
                dimBG.alpha += 0.04f;
            }
        }
        else
        {
            if (dimBG.alpha > 0)
            {
                dimBG.alpha -= 0.04f;
            }
        }
    }


    public void Close()
    {
        _enabled = false;
        _canvasGroup.interactable = false;
    }

    static string ComputeSha256Hash(string rawData)
    {
        using (SHA256 sha256Hash = SHA256.Create())
        {
            byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }

            return builder.ToString();
        }
    }
}