using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class RoomEntry : MonoBehaviour
{
    public string roomName;
    public string roomMaster;
    [HideInInspector] public string roomId;

    [Range(0, 4)] public int playerCount;

    public bool selected;

    private GameObject _roomNameObject;
    private GameObject _roomMasterObject;
    private GameObject _roomCountObject;

    private Text _roomNameText;
    private Text _roomMasterText;
    private Text _roomCountText;

    private GameObject _selectedImage;

    private readonly Vector3 _globalScaleIdle = new Vector3(0.95f, 0.95f, 0.95f);
    private readonly Vector3 _globalScaleSelected = Vector3.one;

    private bool _fullyReady;

    void Start()
    {
        _fullyReady = TryAssign();
    }

    void OnEnable()
    {
        _fullyReady = TryAssign();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        _roomNameText.text = roomName;
        _roomMasterText.text = roomMaster;
        _roomCountText.text = playerCount + "/4";
        
#if UNITY_EDITOR
        if (selected)
        {
            _selectedImage.SetActive(true);
            transform.localScale = _globalScaleSelected;
            _roomNameObject.transform.localScale = _globalScaleIdle;
            _roomMasterObject.transform.localScale = _globalScaleIdle;
            _roomCountObject.transform.localScale = _globalScaleIdle;
        }
        else
        {
            _selectedImage.SetActive(false);
            transform.localScale = _globalScaleIdle;
            _roomNameObject.transform.localScale = _globalScaleSelected;
            _roomMasterObject.transform.localScale = _globalScaleSelected;
            _roomCountObject.transform.localScale = _globalScaleSelected;
        }
#endif
    }

    private void FixedUpdate()
    {
        if (!_fullyReady)
            _fullyReady = TryAssign();
    }

    public void SetSelected(bool value)
    {
        selected = value;
        if (selected)
        {
            _selectedImage.SetActive(true);
            transform.localScale = _globalScaleSelected;
            _roomNameObject.transform.localScale = _globalScaleIdle;
            _roomMasterObject.transform.localScale = _globalScaleIdle;
            _roomCountObject.transform.localScale = _globalScaleIdle;
        }
        else
        {
            _selectedImage.SetActive(false);
            transform.localScale = _globalScaleIdle;
            _roomNameObject.transform.localScale = _globalScaleSelected;
            _roomMasterObject.transform.localScale = _globalScaleSelected;
            _roomCountObject.transform.localScale = _globalScaleSelected;
        }
    }

    public bool TryAssign()
    {
        try
        {
            _selectedImage = transform.GetChild(0).gameObject;
            _roomNameObject = transform.GetChild(1).gameObject;
            _roomMasterObject = transform.GetChild(2).gameObject;
            _roomCountObject = transform.GetChild(3).gameObject;

            _roomNameText = _roomNameObject.GetComponent<Text>();
            _roomMasterText = _roomMasterObject.GetComponent<Text>();

            foreach (Component component in _roomCountObject.GetComponents<Component>())
            {
                if (component is Text text)
                {
                    _roomCountText = text;
                }
            }

            return _selectedImage != null && _roomNameObject != null && _roomMasterObject != null &&
                   _roomCountObject != null && _roomNameText != null && _roomMasterText != null &&
                   _roomCountText != null;
        }
        catch (Exception)
        {
            return false;
        }
    }
}