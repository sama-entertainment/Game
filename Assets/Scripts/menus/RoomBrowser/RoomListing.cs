using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using menus.UIComponents;
using network;
using Photon.Pun;
using Photon.Realtime;
using sound;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace menus.RoomBrowser
{
    public class RoomListing : MonoBehaviourPunCallbacks
    {
        public GameObject entryPrefab;

        public GameObject listContainer;
        public RoomPreview roomPreview;
        public RoomListManager roomListingManager;
        public int lobbySceneID;
        public CanvasGroup[] canvasGroupsToDisable;
        public Button[] buttonsToDisable;
        public PaswordInputPopup passwordPopup;
        public JoinNamedRoomPopup joinNamedRoomPopup;

        private List<RoomEntry> _roomEntries;

        public Button bigJoinButton;

        private string _selected;
        private bool _connecting;
        private bool _failed;
        private int _failedCooldown;

        [CanBeNull]
        public RoomInfo SelectedRoom
        {
            get
            {
                try
                {
                    return roomListingManager.RoomList[_selected];
                }
                catch (NullReferenceException)
                {
                    return null;
                }
                catch (KeyNotFoundException)
                {
                    return null;
                }
            }
        }

        private int _frameCounter;

        // Start is called before the first frame update
        void Start()
        {
            _roomEntries = new List<RoomEntry>();
            _selected = "";
            _frameCounter = 0;
            _connecting = false;
            _failed = true;
            AudioManager.CurrentManager.StartBgMusic();
        }

        void LateUpdate()
        {
            bigJoinButton.interactable = roomPreview.show && !passwordPopup.Opened && !joinNamedRoomPopup.Opened;
        }

        void FixedUpdate()
        {
            if (_failed)
            {
                if (_failedCooldown == 100)
                {
                    _failed = false;
                    MKButton button = roomPreview.joinButton.GetComponent<MKButton>();
                    button.content = "Rejoindre";
                    button.buttonColor = MKButtonColor.GREEN;
                }
                else
                {
                    _failedCooldown++;
                }
            }
            else
            {
                _failedCooldown = 0;
            }

            if (_frameCounter >= 5)
            {
                _frameCounter = 0;
            }


            if (_frameCounter == 0)
            {
                UpdateEntries();
                UpdatePreview();
            }

            _frameCounter++;
        }

        private void UpdateEntries()
        {
            string[] roomsIds = new string[roomListingManager.RoomList.Count];
            roomListingManager.RoomList.Keys.CopyTo(roomsIds, 0);
            int i = 0;
            for (; i < _roomEntries.Count && i < roomsIds.Length; i++)
            {
                UpdateEntry(_roomEntries[i], roomsIds[i]);
            }

            if (i >= _roomEntries.Count)
            {
                for (; i < roomsIds.Length; i++)
                {
                    AddEntry(roomsIds[i]);
                }
            }
            else if (i >= roomsIds.Length)
            {
                for (; i < _roomEntries.Count; i++)
                {
                    DestroyEntry(_roomEntries[i]);
                }
            }
        }

        public void DisableMainButtons()
        {
            foreach (CanvasGroup canvasGroup in canvasGroupsToDisable)
            {
                canvasGroup.interactable = false;
            }

            foreach (Button button in buttonsToDisable)
            {
                button.interactable = false;
            }
        }

        public void EnableMainButtons()
        {
            foreach (CanvasGroup canvasGroup in canvasGroupsToDisable)
            {
                canvasGroup.interactable = true;
            }

            foreach (Button button in buttonsToDisable)
            {
                button.interactable = true;
            }
        }

        private void UpdatePreview()
        {
            roomPreview.show = SelectedRoom != null;
            roomPreview.titleDisplay.text =
                (string) SelectedRoom?.CustomProperties["Name"] ?? "Séléctionner une partie";
            roomPreview.mikuniAmountDisplay.text = SelectedRoom != null
                ? ((int) SelectedRoom.CustomProperties["Mikunis"]).ToString()
                : "N/A";
            roomPreview.masterDislplay.text = (string) SelectedRoom?.CustomProperties["Master"] ?? "N/A";
            roomPreview.timeDisplay.text =
                SelectedRoom != null ? (int) SelectedRoom.CustomProperties["Time"] / 60 + ":00" : "--:--";
            string password = (string) SelectedRoom?.CustomProperties["Password"];
            roomPreview.visibilityDipslay.text = "Public" + (String.IsNullOrWhiteSpace(password) ? "" : " avec code");
        }

        public void AttemptJoinRoom()
        {
            DisableMainButtons();
            _failed = false;
            if (!String.IsNullOrEmpty((string) SelectedRoom?.CustomProperties["Password"]))
            {
                passwordPopup.Open();
                return;
            }

            _connecting = true;
            PhotonNetwork.JoinRoom(_selected);
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            _connecting = false;
            MKButton button = roomPreview.joinButton.GetComponent<MKButton>();
            if (!button.transform.GetChild(0).gameObject.GetComponent<Button>().interactable) return;
            _failed = true;
            EnableMainButtons();
            button.content = "ERREUR";
            button.buttonColor = MKButtonColor.RED;
        }

        public override void OnJoinedRoom()
        {
            _connecting = false;
            MKButton button = roomPreview.joinButton.GetComponent<MKButton>();
            if (!button.transform.GetChild(0).gameObject.GetComponent<Button>().interactable) return;
            EnableMainButtons();
        }

        private void AddEntry(string roomId)
        {
            GameObject entryObject = Instantiate(entryPrefab, listContainer.transform);
            RoomEntry entry = entryObject.GetComponent<RoomEntry>();
            _roomEntries.Add(entry);

            UpdateEntry(entry, roomId);
        }

        private void UpdateEntry(RoomEntry entry, string roomId)
        {
            RoomInfo info = roomListingManager.RoomList[roomId];
            entry.roomName = (string) info.CustomProperties["Name"];
            entry.playerCount = info.PlayerCount;
            entry.roomId = info.Name;
            entry.SetSelected(_selected == entry.roomId);
            entry.roomMaster = (string) info.CustomProperties["Master"];

            Button button = entry.gameObject.GetComponent<Button>();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => _selected = entry.roomId);
        }

        private void DestroyEntry(RoomEntry entry)
        {
            _roomEntries.Remove(entry);
            Destroy(entry.gameObject);
        }
    }
}