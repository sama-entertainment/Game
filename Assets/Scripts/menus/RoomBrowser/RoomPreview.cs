using System;
using System.Collections;
using System.Collections.Generic;
using network;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RoomPreview : MonoBehaviour
{
    public Text titleDisplay;
    
    public Text masterDislplay;
    public Text timeDisplay;
    public Text visibilityDipslay;
    public Text mikuniAmountDisplay;

    public GameObject statsObject;

    public GameObject joinButton;

    public bool show;
    public SlideInRightAnimation mapPreviewAnimation;

    public Button.ButtonClickedEvent onJoinClick => _joinButton.onClick;

    
    private EaseBackAnimation _statsAnimation;
    private EaseBackAnimation _joinButtonAnimation;
    
    private Button _joinButton;

    private CanvasGroup _canvasGroup;

    // Start is called before the first frame update
    void Start()
    {
        _statsAnimation ??= statsObject.GetComponent<EaseBackAnimation>();
        _joinButtonAnimation ??= joinButton.GetComponent<EaseBackAnimation>();
        _joinButton ??= joinButton.transform.GetChild(0).gameObject.GetComponent<Button>();
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        _statsAnimation.enable = show;
        _joinButtonAnimation.enable = show;
        _canvasGroup.interactable = show;
        mapPreviewAnimation.enable = show;
    }

    private void FixedUpdate()
    {
        _statsAnimation ??= statsObject.GetComponent<EaseBackAnimation>();
        _joinButtonAnimation ??= joinButton.GetComponent<EaseBackAnimation>();
        _joinButton ??= joinButton.transform.GetChild(0).gameObject.GetComponent<Button>();
    }
}