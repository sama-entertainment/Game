using System;
using network;
using Photon.Pun;
using sound;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace menus
{
    public class SettingsManager : MonoBehaviour
    {
        private Resolution[] _resolutions;

        public GameObject resolutionDisplay;
        public GameObject windowedModeToggle;
        public GameObject usernameInput;

        public GameObject masterVolumeSlider;
        public GameObject musicVolumeSlider;
        public GameObject sfxVolumeSlider;
        
        private int _resolutionIndex;
        private TextMeshProUGUI _textDisplay;
        private bool _windowed;

        public string Username
        {
            private get => PlayerInfo.PInfo.username;
            set => PlayerInfo.PInfo.username = value;
        }

        // Start is called before the first frame update
        void Start()
        {
            QualitySettings.vSyncCount = 1;
            Application.targetFrameRate = 75;
            _resolutions = Screen.resolutions;
            _textDisplay = resolutionDisplay.GetComponent<TextMeshProUGUI>();
            ReloadSettings(true);
        }

        public void ReloadSettings(bool apply)
        {
            Resolution prefResolution = GetPreferredResolutionOrDefault();
            _resolutionIndex = BinarySearchIndexOf(prefResolution, _resolutions);
            _textDisplay.text = prefResolution.ToString();
            Username = PlayerPrefs.GetString("OnlineUsername", null);
            if (Username != null) usernameInput.GetComponent<TMP_InputField>().text = Username;

            masterVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MasterVolume", 1);
            musicVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicVolume", 1);
            sfxVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("SFXVolume", 1);

            _windowed = PlayerPrefs.GetInt("WindowedMode", 0) == 1;
            windowedModeToggle.GetComponent<Toggle>().isOn = _windowed;
            
            if (apply)
            {
                AudioManager.CurrentManager.MasterVolume = masterVolumeSlider.GetComponent<Slider>().value;
                AudioManager.CurrentManager.MusicVolume = musicVolumeSlider.GetComponent<Slider>().value;
                AudioManager.CurrentManager.SFXVolume = sfxVolumeSlider.GetComponent<Slider>().value;
                Screen.SetResolution(prefResolution.width, prefResolution.height,
                    _windowed ? FullScreenMode.Windowed : FullScreenMode.ExclusiveFullScreen);
            }
        }

        private void OnEnable()
        {
            string username = PlayerPrefs.GetString("OnlineUsername", null);
            if (username != null)
            {
                PlayerInfo.PInfo.username = username;
                usernameInput.GetComponent<TMP_InputField>().text = username;
            }
        }

        public void SetNextResolution()
        {
            _resolutionIndex++;
            if (_resolutionIndex >= _resolutions.Length)
                _resolutionIndex = 0;
            Resolution fetchedRes = _resolutions[_resolutionIndex];
            _textDisplay.text = fetchedRes.ToString();
            Debug.Log(Screen.currentResolution.ToString());
        }

        public void SetPrevResolution()
        {
            _resolutionIndex--;
            if (_resolutionIndex < 0)
                _resolutionIndex = _resolutions.Length - 1;
            Resolution fetchedRes = _resolutions[_resolutionIndex];
            _textDisplay.text = fetchedRes.ToString();
            Debug.Log(Screen.currentResolution.ToString());
        }

        public void ApplyResolution()
        {
            _windowed = windowedModeToggle.GetComponent<Toggle>().isOn;
            PlayerPrefs.SetString("GameResolution", _resolutions[_resolutionIndex].ToString());
            PlayerPrefs.SetInt("WindowedMode", _windowed ? 1 : 0);
            Resolution fetchedRes = _resolutions[_resolutionIndex];
            Screen.SetResolution(fetchedRes.width, fetchedRes.height,
                _windowed ? FullScreenMode.Windowed : FullScreenMode.ExclusiveFullScreen);
        }

        public void RefreshRes()
        {
            _resolutions = Screen.resolutions;
            if (_resolutionIndex >= _resolutions.Length) _resolutionIndex = _resolutions.Length - 1;
        }

        private bool resGT(Resolution lh, Resolution rh)
        {
            return lh.width > rh.width || lh.height > rh.height;
        }

        private bool resGEqT(Resolution lh, Resolution rh)
        {
            return lh.width >= rh.width || lh.height >= rh.height;
        }

        private bool resLT(Resolution lh, Resolution rh)
        {
            return lh.width < rh.width || lh.height < rh.height;
        }

        private bool resLEqT(Resolution lh, Resolution rh)
        {
            return lh.width <= rh.width || lh.height <= rh.height;
        }

        private bool resEq(Resolution lh, Resolution rh)
        {
            return lh.width == rh.width && lh.height == rh.height;
        }

        private int BinarySearchIndexOf(Resolution res, Resolution[] array)
        {
            int l = 0;
            int r = array.Length - 1;
            int m;

            do
            {
                m = (l + r) / 2;
                if (resEq(res, array[m]))
                {
                    break;
                }

                if (resLT(res, array[m]))
                {
                    r = m - 1;
                }
                else
                {
                    l = m + 1;
                }
            } while (l <= r);

            return m;
        }

        private Resolution GetPreferredResolutionOrDefault()
        {
            string prefRes = PlayerPrefs.GetString("GameResolution", "");

            Resolution resolution;

            if (prefRes != "")
            {
                string[] res = prefRes.Replace(" ", String.Empty).Split('@')[0].Split('x');

                resolution = new Resolution
                {
                    width = Int32.Parse(res[0]),
                    height = Int32.Parse(res[1])
                };
            }
            else
            {
                resolution = Screen.currentResolution;
            }

            return resolution;
        }

        public void ApplyUsername()
        {
            Username = usernameInput.GetComponent<TMP_InputField>().text;
            PlayerPrefs.SetString("OnlineUsername", Username);
            Debug.Log("Applied username: " + Username);
        }

        public void ApplyVolumePreferences()
        {
            PlayerPrefs.SetFloat("MasterVolume", masterVolumeSlider.GetComponent<Slider>().value);
            PlayerPrefs.SetFloat("MusicVolume", musicVolumeSlider.GetComponent<Slider>().value);
            PlayerPrefs.SetFloat("SFXVolume", sfxVolumeSlider.GetComponent<Slider>().value);
        }
    }
}