using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace menus.Lobby
{
    public class PasswordDisplay : MonoBehaviour
    {
        public InputField passwordField;

        private Text _display;
        private string _statusText;
        // Start is called before the first frame update
        void Start()
        {
            _display = GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            _display.text = _statusText;
        }

        private void LateUpdate()
        {
            _statusText = string.IsNullOrEmpty((string) PhotonNetwork.CurrentRoom?.CustomProperties?["Password"])
                ? "Sans" : "Avec";
        }
    }
}
