﻿using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace menus.Lobby
{
    public class TimeDisplay : MonoBehaviour
    {
        private Text _text;

        private void Start()
        {
            _text = GetComponent<Text>();
        }
        
        private void LateUpdate()
        {
            if (PhotonNetwork.CurrentRoom?.CustomProperties?["Time"] == null) return;
            int totalTime = (int) PhotonNetwork.CurrentRoom.CustomProperties["Time"];
            int minutes = totalTime / 60;
            int seconds = totalTime % 60;
            _text.text = $"{minutes}:{seconds.ToString().PadLeft(2, '0')}";
        }
    }
}