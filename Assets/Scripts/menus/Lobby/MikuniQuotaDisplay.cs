using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MikuniQuotaDisplay : MonoBehaviour
{

    private Text _counter;

    public int Value
    {
        get => Int32.Parse(_counter.text);
        set => _counter.text = value + "";
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _counter = transform.GetChild(0).gameObject.GetComponent<Text>();
    }
}
