using System;
using System.Collections.Generic;
using menus.UIComponents;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

namespace menus.Lobby
{
    public class PlayerKickButtons : MonoBehaviour
    {
        public List<Player> players;

        private Transform _buttonSetTransform;

        // Start is called before the first frame update
        void Start()
        {
            _buttonSetTransform = transform.GetChild(1);
        }

        // Update is called once per frame
        void LateUpdate()
        {
            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            int i = 0;
            int len = Math.Min(players.Count, 4);
            for (; i < len; i++)
            {
                Player pl = players[i];
                GameObject buttonObject = _buttonSetTransform.GetChild(i).gameObject;
                MKButton button = buttonObject.GetComponent<MKButton>();
                Button legacyButton = buttonObject.transform.GetChild(0).gameObject.GetComponent<Button>();

                if (actorNumber == pl.ActorNumber)
                {
                    button.buttonColor = MKButtonColor.GRAY;
                    legacyButton.interactable = false;
                }
                else
                {
                    button.buttonColor = MKButtonColor.PURPLE;
                    legacyButton.interactable = true;
                }

                button.content = players[i].NickName;
            }

            for (; i < 4; i++)
            {
                GameObject buttonObject = _buttonSetTransform.GetChild(i).gameObject;
                MKButton button = buttonObject.GetComponent<MKButton>();
                Button legacyButton = buttonObject.transform.GetChild(0).gameObject.GetComponent<Button>();

                button.content = "...";
                button.buttonColor = MKButtonColor.GRAY;
                legacyButton.interactable = false;
            }
        }
    }
}