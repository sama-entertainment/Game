using menus.UIComponents;
using Photon.Pun;
using UnityEngine;

namespace menus.Lobby
{
    public class TimeSetter : MonoBehaviour
    {
        [Range(0, 2)]
        public int timeIndex;

        void Update()
        {
            for (int i = 0; i < 3; i++)
            {
                transform.GetChild(i).GetComponent<MKButton>().buttonColor =
                    i == timeIndex ? MKButtonColor.GREEN : MKButtonColor.GRAY;
            }
        }

        private void LateUpdate()
        {
            if (PhotonNetwork.CurrentRoom?.CustomProperties?["Time"] == null)
                return;
            int time = (int) PhotonNetwork.CurrentRoom.CustomProperties["Time"];
            if (time <= 180)
            {
                timeIndex = 0;
            }
            else if (time <= 360)
            {
                timeIndex = 1;
            }
            else
            {
                timeIndex = 2;
            }
        }
    }
}
