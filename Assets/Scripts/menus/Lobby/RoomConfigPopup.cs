using System;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

namespace menus.Lobby
{
    public class RoomConfigPopup : MonoBehaviour
    {
        public EditableObject editableName;
        public EditableObject editablePassword;
        public EditableObject editableTime;
        public EditableObject editableMikuniQuota;
        public GameObject playerKickSection;
        public GameObject popupObject;
        public Button[] buttonsToDisable;

        private PlayerKickButtons _buttons;
        private bool _enabled;
        private CanvasGroup _canvasGroup;
        private EaseBackAnimation _popupAnimation;
        private Text _nameDisplay;
        private Text _timeDisplay;
        private Text _mikuniCounter;
        private TimeSetter _timeSetter;
        public CanvasGroup dimBG;
        private bool _fullyLoaded;

        public void Open()
        {
            _enabled = true;
            _canvasGroup.interactable = true;
            foreach (Button button in buttonsToDisable)
            {
                button.interactable = false;
            }
        }

        public string RoomName
        {
            get => editableName.EditModeGameObject.GetComponent<InputField>().text;
            set
            {
                editableName.EditModeGameObject.GetComponent<InputField>().text = value;
                editableName.DisplayModeGameObject.GetComponent<Text>().text = value;
            }
        }

        public string Password
        {
            get => editablePassword.EditModeGameObject.GetComponent<InputField>().text;
            set => editablePassword.EditModeGameObject.GetComponent<InputField>().text = value;
        }


        public List<Player> Players
        {
            set => _buttons.players = value;
        }

        // Start is called before the first frame update
        void Awake()
        {
            _enabled = false;
            _canvasGroup = GetComponent<CanvasGroup>();
            _canvasGroup.interactable = true;
            _buttons = playerKickSection.GetComponent<PlayerKickButtons>();
            _popupAnimation = popupObject.GetComponent<EaseBackAnimation>();
            _fullyLoaded = ReassignObjects();
        }

        private void LateUpdate()
        {
            if (PhotonNetwork.CurrentRoom?.CustomProperties == null) // No longer in a room
                return;
            if (!_fullyLoaded)
                _fullyLoaded = ReassignObjects();
            _popupAnimation.enable = _enabled;
            editableName.editMode = PhotonNetwork.IsMasterClient;
            editablePassword.editMode = PhotonNetwork.IsMasterClient;
            editableTime.editMode = PhotonNetwork.IsMasterClient;
            editableMikuniQuota.editMode = PhotonNetwork.IsMasterClient;
            playerKickSection.SetActive(PhotonNetwork.IsMasterClient);
            _nameDisplay.text = (string) PhotonNetwork.CurrentRoom.CustomProperties["Name"];
            _timeDisplay.text = _timeSetter.timeIndex switch
            {
                0 => "3:00",
                1 => "6:00",
                2 => "9:00",
                _ => "???"
            };
            _mikuniCounter.text = ((int) (PhotonNetwork.CurrentRoom.CustomProperties["Mikunis"] ?? 727)).ToString();
        }

        private void FixedUpdate()
        {
            if (_enabled)
            {
                if (dimBG.alpha < 1f)
                {
                    dimBG.alpha += 0.04f;
                }
            }
            else if (dimBG.alpha > 0)
            {
                dimBG.alpha -= 0.04f;
            }
        }

        private bool ReassignObjects()
        {
            try
            {
                _nameDisplay = editableName.DisplayModeGameObject.GetComponent<Text>();
                _timeDisplay = editableTime.DisplayModeGameObject.GetComponent<Text>();
                _timeSetter = editableTime.EditModeGameObject.GetComponent<TimeSetter>();
                _mikuniCounter = editableMikuniQuota.DisplayModeGameObject.transform.GetChild(0).gameObject
                    .GetComponent<Text>();
                return _nameDisplay != null && _timeDisplay != null && _timeSetter != null && _mikuniCounter != null;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        public void Close()
        {
            _enabled = false;
            _canvasGroup.interactable = false;
            foreach (Button button in buttonsToDisable)
            {
                button.interactable = true;
            }
        }
    }
}