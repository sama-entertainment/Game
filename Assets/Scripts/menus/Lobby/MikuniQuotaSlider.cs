using UnityEngine;
using UnityEngine.UI;

namespace menus.Lobby
{
    public class MikuniQuotaSlider : MonoBehaviour
    {

        private Slider _slider;
        private Text _counter;

        public int Value
        {
            get => Mathf.RoundToInt(_slider.value);
            set => _slider.value = value;
        }
        // Start is called before the first frame update
        void Start()
        {
            _slider = transform.GetChild(0).gameObject.GetComponent<Slider>();
            _counter = transform.GetChild(1).gameObject.GetComponent<Text>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            _counter.text = Value + "";
        }
    }
}
