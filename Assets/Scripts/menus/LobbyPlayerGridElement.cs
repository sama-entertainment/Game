using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPlayerGridElement : MonoBehaviour
{
    private EaseBackAnimation _checkmark;
    private Text _text;

    public string playerName;
    public bool ready; 
    
    // Start is called before the first frame update
    void Start()
    {
        _text = transform.GetChild(0).gameObject.GetComponent<Text>();
        _checkmark = transform.GetChild(1).gameObject.GetComponent<EaseBackAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = playerName;
        _checkmark.enable = ready;
    }
}
