using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditableObject : MonoBehaviour
{

    public bool editMode;

    private GameObject _editModeGameObject;
    private GameObject _displayModeGameObject;

    public GameObject EditModeGameObject => _editModeGameObject;
    public GameObject DisplayModeGameObject => _displayModeGameObject;
    
    // Start is called before the first frame update
    void Start()
    {
        _editModeGameObject = transform.GetChild(0).gameObject;
        _displayModeGameObject = transform.GetChild(1).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        _editModeGameObject.SetActive(editMode);
        _displayModeGameObject.SetActive(!editMode);
    }
}
