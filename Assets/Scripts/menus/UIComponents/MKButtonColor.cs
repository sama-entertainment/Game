namespace menus.UIComponents
{
    public enum MKButtonColor
    {
        GREEN = 0,
        PURPLE = 1,
        BLUE = 2,
        RED = 3,
        GRAY = 4
    }
}