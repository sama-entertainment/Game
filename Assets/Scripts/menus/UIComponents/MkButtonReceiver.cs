﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace menus.UIComponents
{
    [RequireComponent(typeof(Button))]
    public class MkButtonReceiver : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public event Action<PointerEventData> OnMouseDown;
        public event Action<PointerEventData> OnMouseUp;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            OnMouseDown?.Invoke(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnMouseUp?.Invoke(eventData);
        }
    }
}