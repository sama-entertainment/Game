using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponButton : MonoBehaviour
{

    private Image _selectedImage;
    private Image _idleImage;
    
    public bool selected;


    // Start is called before the first frame update
    void Start()
    {
        _selectedImage = GetComponent<Image>();
        _idleImage = transform.GetChild(0).gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        _selectedImage.enabled = selected;
        _idleImage.enabled = !selected;
    }
}
