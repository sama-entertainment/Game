using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class MKSlider : MonoBehaviour
{
    public GameObject fillArea;
    [Range(0, 4)] [DefaultValue(4)] public int value;

    private int _realValue;

    private RectTransform _rectTransform;

    private RectMask2D _rectMask2D;

    private int _timer;

    // Start is called before the first frame update
    void Start()
    {
        _rectMask2D = fillArea.GetComponent<RectMask2D>();
        _rectTransform = (RectTransform) fillArea.transform;
        _realValue = value;
    }

    // Update is called once per frame
    void LateUpdate()
    { 
        _rectMask2D.padding = new Vector4(0, 0, _rectTransform.rect.width * ValueToMultiplier(_realValue), 0);
    }

    private void FixedUpdate()
    {
        if (_timer == 0)
        {
            UpdateRealValue();
        }

        _timer++;
        if (_timer >= 10)
        {
            _timer = 0;
        }
    }

    private void UpdateRealValue()
    {
        if (_realValue > value)
        {
            _realValue--;
            return;
        }

        if (_realValue < value)
            _realValue++;
    }

    private float ValueToMultiplier(int value) => value switch
    {
        0 => 1f,
        1 => 0.745f,
        2 => 0.5f,
        3 => 0.26f,
        _ => 0f
    };
}