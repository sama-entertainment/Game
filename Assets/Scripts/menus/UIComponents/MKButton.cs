using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace menus.UIComponents
{
    public class MKButton : MonoBehaviour
    {
        private GameObject ButtonObject => transform.GetChild(0).gameObject;
        private GameObject TextObject => transform.GetChild(0).GetChild(0).gameObject;
        private float _animProgress;
        private bool _pressed;

        private Button Button => ButtonObject.GetComponent<Button>();
        private Text Text => TextObject.GetComponent<Text>();
        private MkButtonReceiver _receiver;

        [Range(1, 50)] public float animationSpeed = 10;

        public string content;
        public MKButtonColor buttonColor;

        private void Awake()
        {
            Update();
            _receiver = GetComponentInChildren<MkButtonReceiver>();
            _receiver.OnMouseUp += OnPointerUp;
            _receiver.OnMouseDown += OnPointerDown;
        }

        // Start is called before the first frame update
        void Start()
        {
            Text.color = Color.white;
        }

        public void Update()
        {
            Text.text = content;
            Button.colors = MKUtils.MkColorToColorBlock(buttonColor);
        }

        void LateUpdate()
        {
            Button.transform.localScale = Vector3.one *
                                          (Button.interactable
                                              ? _pressed
                                                  ? Mathf.LerpUnclamped(1, 0.95f, EaseOutElastic(_animProgress))
                                                  : Mathf.LerpUnclamped(0.95f, 1, EaseOutElastic(1 - _animProgress))
                                              : 1f);
        }

        private void FixedUpdate()
        {
            if (_pressed)
            {
                if (_animProgress < 1f)
                {
                    _animProgress += 1f / (100 - animationSpeed + 1);
                }
            }
            else
            {
                if (_animProgress > 0f)
                {
                    _animProgress -= 1f / (100 - animationSpeed + 1);
                }
            }
        }

        private float EaseOutElastic(float input)
        {
            float c4 = 2 * Mathf.PI / 3;

            return input == 0
                ? 0
                : Math.Abs(input - 1) < 0.0001
                    ? 1f
                    : Mathf.Pow(2, -10 * input) * Mathf.Sin((input * 10 - 0.75f) * c4) + 1;
        }

        private void OnPointerDown(PointerEventData eventData)
        {
            _pressed = true;
        }

        private void OnPointerUp(PointerEventData eventData)
        {
            _pressed = false;
        }
    }
}