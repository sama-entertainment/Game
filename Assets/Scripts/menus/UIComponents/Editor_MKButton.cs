#if (UNITY_EDITOR)
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace menus.UIComponents
{
    [CustomEditor(typeof(MKButton))]
    public class Editor_MKButton : Editor
    {
        private MKButton _target;
        private SerializedProperty _contentProperty;
        private SerializedProperty _selectedColorProperty;
        private SerializedProperty _animationSpeedProperty;

        private void Awake()
        {
            _contentProperty = serializedObject.FindProperty("content");
            _selectedColorProperty = serializedObject.FindProperty("buttonColor");
            _animationSpeedProperty = serializedObject.FindProperty("animationSpeed");
            if (_target == null) return;
            _target.content = _contentProperty.stringValue;
            _target.buttonColor = (MKButtonColor) _selectedColorProperty.enumValueIndex;
            _target.animationSpeed = _animationSpeedProperty.floatValue;
            _target.Update();
        }

        private static readonly string[] ColorOptions =
        {
            "Green",
            "Purple",
            "Blue",
            "Red",
            "Gray"
        };

        private void OnSceneGUI()
        {
            _contentProperty = serializedObject.FindProperty("content");
            _selectedColorProperty = serializedObject.FindProperty("buttonColor");
            _animationSpeedProperty = serializedObject.FindProperty("animationSpeed");
            if (_target == null) return;
            _target.Update();
        }

        private void OnEnable()
        {
            _contentProperty = serializedObject.FindProperty("content");
            _selectedColorProperty = serializedObject.FindProperty("buttonColor");
            _animationSpeedProperty = serializedObject.FindProperty("animationSpeed");
            if (_target == null) return;
            _target.Update();
        }

        public override void OnInspectorGUI()
        {
            // base.OnInspectorGUI();
            serializedObject.Update();
            _contentProperty = serializedObject.FindProperty("content");
            _selectedColorProperty = serializedObject.FindProperty("buttonColor");
            _animationSpeedProperty = serializedObject.FindProperty("animationSpeed");
            EditorGUILayout.BeginVertical();
            if (EnsureStructureOkay())
            { 
                _contentProperty.stringValue = EditorGUILayout.TextField("Text content", _contentProperty.stringValue);
                _selectedColorProperty.enumValueIndex =
                    EditorGUILayout.Popup("Color", _selectedColorProperty.enumValueIndex, ColorOptions);
                _animationSpeedProperty.floatValue =
                    EditorGUILayout.Slider("Animation speed", _animationSpeedProperty.floatValue, 1, 50);

                if (GUILayout.Button("Repair style"))
                {
                    SetStyle();
                }
            }
            else
            {
                if (GUILayout.Button("Generate Prefab"))
                {
                    GeneratePrefab();
                }
            }



            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
            if (_target == null) return;
            _target.Update();

        }

        private bool EnsureStructureOkay()
        {
            if (!(target is MKButton mkButton)) return false;

            _target = mkButton;

            if (_target.transform.childCount <= 0) return false;
            GameObject supposedButton = _target.transform.GetChild(0).gameObject;
            if (supposedButton.GetComponent<Button>() == null) return false;
            if (supposedButton.transform.childCount <= 0) return false;
            GameObject supposedText = supposedButton.transform.GetChild(0).gameObject;
            if (supposedText.GetComponent<Text>() == null) return false;

            return true;
        }

        private void GeneratePrefab()
        {
            _target.gameObject.layer = 5;
            //// Button generation
            GameObject buttonGameObject = new GameObject("ButtonObject");
            buttonGameObject.layer = 5;
            // Adjusting the RectTransform and its anchoring
            RectTransform buttonRectTransform = buttonGameObject.AddComponent<RectTransform>();
            buttonRectTransform.SetParent(_target.transform);
            buttonRectTransform.anchorMin = Vector2.zero;
            buttonRectTransform.anchorMax = Vector2.one;
            buttonRectTransform.pivot = Vector2.one * 0.5f;
            buttonRectTransform.localPosition = Vector3.zero;
            buttonRectTransform.sizeDelta = Vector2.zero;
            // Setup Image
            buttonGameObject.AddComponent<CanvasRenderer>().cull = true;
            Image image = buttonGameObject.AddComponent<Image>();
            image.sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Sprites/UI/white_circle.png");
            image.color = Color.white;
            image.raycastTarget = true;
            image.raycastPadding = Vector4.zero;
            image.maskable = true;
            image.type = Image.Type.Sliced;
            image.fillCenter = true;
            image.pixelsPerUnitMultiplier = 3f;
            // Setup button
            Button button = buttonGameObject.AddComponent<Button>();
            button.targetGraphic = image;


            //// Text generation
            GameObject textGameObject = new GameObject("Text");
            textGameObject.layer = 5;
            // RectTransform
            RectTransform textRectTransform = textGameObject.AddComponent<RectTransform>();
            textRectTransform.SetParent(buttonRectTransform.transform);
            textRectTransform.anchorMin = Vector2.zero;
            textRectTransform.anchorMax = Vector2.one;
            textRectTransform.pivot = Vector2.one * 0.5f;
            textRectTransform.localPosition = Vector3.zero;
            // Setup text
            textGameObject.AddComponent<CanvasRenderer>().cull = true;
            Text text = textGameObject.AddComponent<Text>();
            text.text = "Button";
            text.font = AssetDatabase.LoadAssetAtPath<Font>("Assets/Sprites/UI/LuckiestGuy-Regular.ttf");
            text.fontStyle = FontStyle.Normal;
            text.fontSize = 32;
            text.lineSpacing = 1f;
            text.supportRichText = true;
            text.alignment = TextAnchor.MiddleCenter;
            text.alignByGeometry = true;
            text.horizontalOverflow = HorizontalWrapMode.Wrap;
            text.verticalOverflow = VerticalWrapMode.Truncate;
            text.resizeTextForBestFit = false;
            text.color = Color.white;
            text.raycastTarget = true;
            text.raycastPadding = Vector4.zero;
            text.maskable = true;
        }

        private void SetStyle()
        {
            Transform baseTransform = _target.transform;
            GameObject buttonObject = baseTransform.GetChild(0).gameObject;
            RectTransform buttonRectTransform = (RectTransform) buttonObject.transform;
            buttonRectTransform.localPosition = Vector3.zero;
            buttonRectTransform.anchorMin = Vector2.zero;
            buttonRectTransform.anchorMax = Vector2.one;
            buttonRectTransform.pivot = Vector2.one * 0.5f;
            buttonRectTransform.sizeDelta = Vector2.zero;

            Image image = buttonObject.GetComponent<Image>();

            image.sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Sprites/UI/white_circle.png");
            image.color = Color.white;
            image.raycastTarget = true;
            image.raycastPadding = Vector4.zero;
            image.maskable = true;
            image.type = Image.Type.Sliced;
            image.fillCenter = true;
            image.pixelsPerUnitMultiplier = 3f;


            GameObject textObject = buttonObject.transform.GetChild(0).gameObject;
            RectTransform textRectTransform = (RectTransform) textObject.transform;
            Text text = textObject.GetComponent<Text>();
            textRectTransform.anchorMin = Vector2.zero;
            textRectTransform.anchorMax = Vector2.one;
            textRectTransform.pivot = Vector2.one * 0.5f;
            textRectTransform.localPosition = Vector3.zero;

            text.text = "Button";
            text.font = AssetDatabase.LoadAssetAtPath<Font>("Assets/Sprites/UI/LuckiestGuy-Regular.ttf");
            text.fontStyle = FontStyle.Normal;
            text.fontSize = 32;
            text.lineSpacing = 1f;
            text.supportRichText = true;
            text.alignment = TextAnchor.MiddleCenter;
            text.alignByGeometry = true;
            text.horizontalOverflow = HorizontalWrapMode.Wrap;
            text.verticalOverflow = VerticalWrapMode.Truncate;
            text.resizeTextForBestFit = false;
            text.color = Color.white;
            text.raycastTarget = true;
            text.raycastPadding = Vector4.zero;
            text.maskable = true;
        }
    }
}
#endif