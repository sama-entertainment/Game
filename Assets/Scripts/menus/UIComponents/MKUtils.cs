using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace menus.UIComponents
{
    public static class MKUtils
    {
        public static ColorBlock MkColorToColorBlock(MKButtonColor color)
        {
            return color switch
            {
                MKButtonColor.PURPLE => MKColorBlockFromTwo(ColorFrom255BasedRGBA(180, 127, 184),
                    ColorFrom255BasedRGBA(165, 117, 168)),
                MKButtonColor.BLUE => MKColorBlockFromTwo(ColorFrom255BasedRGBA(115, 187, 210),
                    ColorFrom255BasedRGBA(105, 170, 191)),
                MKButtonColor.RED => MKColorBlockFromTwo(ColorFrom255BasedRGBA(213, 93, 93),
                    ColorFrom255BasedRGBA(194, 85, 85)),
                MKButtonColor.GRAY => MKColorBlockFromTwo(ColorFrom255BasedRGBA(196, 196, 196),
                    ColorFrom255BasedRGBA(187, 187, 187)),
                _ => MKColorBlockFromTwo(ColorFrom255BasedRGBA(145, 213, 93), ColorFrom255BasedRGBA(136, 199, 87))
            };
        }

        public static Color ColorFrom255BasedRGBA(int r, int g, int b, int a = 255)
        {
            return new Color(
                r / 255f,
                g / 255f,
                b / 255f,
                a / 255f
            );
        }

        public static ColorBlock MKColorBlockFromTwo(Color upColor, Color downColor)
            => new ColorBlock()
            {
                normalColor = upColor,
                selectedColor = upColor,
                highlightedColor = downColor,
                pressedColor = downColor,
                disabledColor = ColorFrom255BasedRGBA(196, 196, 196),
                colorMultiplier = 1f,
                fadeDuration = 0.1f
            };

        public static void EditColorBlock(ref ColorBlock initial, ColorBlock edited)
        {
            initial.colorMultiplier = edited.colorMultiplier;
            initial.disabledColor = edited.disabledColor;
            initial.fadeDuration = edited.fadeDuration;
            initial.highlightedColor = edited.highlightedColor;
            initial.normalColor = edited.normalColor;
            initial.pressedColor = edited.pressedColor;
            initial.selectedColor = edited.selectedColor;
        }

        public static void SetPrivateField(object obj, string name, object value)
        {
            Type type = obj.GetType();
            FieldInfo field = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);
            field?.SetValue(value, obj);
        }

        public static object GetPrivateField(object obj, string name)
        {
            Type type = obj.GetType();
            FieldInfo field = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);
            return field?.GetValue(obj);
        }

        public static void SetPrivateProperty(object obj, string name, object value)
        {
            Type type = obj.GetType();
            PropertyInfo property = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Instance);
            property?.SetValue(value, obj);
        }

        public static object GetPrivateProperty(object obj, string name)
        {
            Type type = obj.GetType();
            PropertyInfo property = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Instance);
            return property?.GetValue(obj);
        }
    }
}