using System;
using System.Collections;
using System.Collections.Generic;
using sound;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace menus
{
    public class UIManager : MonoBehaviour
    {
        public Camera mainCamera;

        public GameObject titleGroupObject;
        public GameObject playGroupObject;
        public GameObject loadingGroupObject;
        public GameObject settingsGroupObject;
        public CanvasGroup keybindsGroupObject;

        public GameObject cauldronObject;

        public int creditsSceneId;
        private Vector3 _cauldronTitlePos = new Vector3(23.8f, -24f, -3f);
        private readonly Vector3 _cauldronPlayPos = new Vector3(23.8f, -18.3f, 9.5f);
        private Vector3 _targetDirection;
        private Vector3 _nextDirection;
        private Quaternion _cauldronPlayRot;
        private Quaternion _cauldronTitleRot;
        private CanvasGroup _titleCanvasGroup;
        private CanvasGroup _playCanvasGroup;
        private CanvasGroup _loadingCanvasGroup;

        private AsyncOperation _sceneLoading;

        private bool _playMenu;
        private bool _settingsMenu;
        private bool _keybindsMenu;
        private float _camProgression = 0;
        private float _titleAlphaProgression = 0;
        private float _playAlphaProgression = 0;
        private float _loadingScreenAlphaProgression = 0;

        public void PlayGame()
        {
            _playMenu = true;
        }

        public void ReturnBack()
        {
            _playMenu = false;
        }

        public void Start()
        {
            _cauldronPlayRot = Quaternion.Euler(90, 0, 0);
            _cauldronTitleRot = Quaternion.Euler(15, 0, 0);
            _playMenu = false;
            _camProgression = 0;
            _titleAlphaProgression = 1;
            _playAlphaProgression = 0;
            _loadingScreenAlphaProgression = 0;
            _titleCanvasGroup = titleGroupObject.GetComponent<CanvasGroup>();
            _playCanvasGroup = playGroupObject.GetComponent<CanvasGroup>();
            _loadingCanvasGroup = loadingGroupObject.GetComponent<CanvasGroup>();
            mainCamera.transform.position = _cauldronTitlePos;
            AudioManager.CurrentManager.StartBgMusic();
        }

        public void FixedUpdate()
        {
            UpdatePlay();

            UpdateTitle();

            UpdateLoadingScreen();

            UpdateSettings();
            
            UpdateKeybinds();

            _cauldronTitleRot =
                Quaternion.LookRotation(cauldronObject.transform.position - mainCamera.transform.position);
            _titleCanvasGroup.alpha = EaseInOutSine(1 - _titleAlphaProgression);
            _playCanvasGroup.alpha = EaseInOutSine(_playAlphaProgression);
            _loadingCanvasGroup.alpha = _loadingScreenAlphaProgression;
        }

        public void LateUpdate()
        {
            Vector3 newPos =
                Vector3.Slerp(_cauldronTitlePos, _cauldronPlayPos, EaseInOutSine(_camProgression));
            newPos.x = _cauldronTitlePos.x;
            mainCamera.transform.position = newPos;
            mainCamera.transform.rotation =
                Quaternion.Lerp(_cauldronTitleRot, _cauldronPlayRot, EaseInOutSine(_camProgression));

            if (_sceneLoading != null && _sceneLoading.progress >= 0.9f)
            {
                _sceneLoading.allowSceneActivation = true;
            }
        }

        private void UpdateTitle()
        {
            _titleCanvasGroup.interactable = !(_playMenu || _settingsMenu);
            if (_playMenu)
            {
                if (_camProgression > 1)
                    titleGroupObject.SetActive(false);
                if (_titleAlphaProgression < 1)
                    _titleAlphaProgression += 0.05f;
            }
            else
            {
                if (_camProgression <= 0)
                {
                    titleGroupObject.SetActive(true);
                    if (_titleAlphaProgression > 0)
                        _titleAlphaProgression -= 0.05f;
                }
            }

            titleGroupObject.SetActive(!_settingsMenu);
        }


        private void UpdatePlay()
        {
            if (_playMenu)
            {
                playGroupObject.SetActive(true);
                if (_camProgression <= 1)
                    _camProgression += 0.01f;
                else
                {
                    _playCanvasGroup.interactable = true;
                }

                if (_playAlphaProgression < 1)
                    _playAlphaProgression += 0.01f;

            }
            else
            {
                _playCanvasGroup.interactable = false;
                if (_camProgression > 0)
                    _camProgression -= 0.01f;
                else
                {
                    playGroupObject.SetActive(false);
                }

                if (_playAlphaProgression > 0)
                    _playAlphaProgression -= 0.01f;
            }
        }

        private void UpdateSettings()
        {
            settingsGroupObject.SetActive(_settingsMenu);
        }

        private void UpdateKeybinds()
        {
            keybindsGroupObject.gameObject.SetActive(_keybindsMenu);
        }

        private void UpdateLoadingScreen()
        {
            if (_sceneLoading != null)
            {
                loadingGroupObject.SetActive(true);
                if (_loadingScreenAlphaProgression < 1)
                    _loadingScreenAlphaProgression += 0.1f;

                loadingGroupObject.GetComponentInChildren<TextMeshProUGUI>().text =
                    $"Chargement...\n{Math.Ceiling(_sceneLoading.progress / 0.9):P}";
            }
            else
            {
                if (_loadingScreenAlphaProgression > 0)
                    _loadingScreenAlphaProgression -= 0.1f;
                else
                    loadingGroupObject.SetActive(false);
            }
        }

        public void OpenSettings()
        {
            _settingsMenu = true;
        }

        public void CloseSettings()
        {
            _settingsMenu = false;
        }

        public void SetKeybindsMenuState(bool show)
        {
            _keybindsMenu = show;
        }

        public void OpenCredits()
        {
             SceneManager.LoadSceneAsync(creditsSceneId);
        }

        public void QuitGame()
        {
            Debug.Log("=> Quit");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }

        private float EaseInOutSine(float x) => (float)((1 - Math.Cos(Math.PI * x)) / 2);
    }
}
