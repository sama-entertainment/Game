using System.Collections.Generic;
using network.controllers;
using player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace menus
{
    public class PlayerHUD : MonoBehaviour
    {
        public static PlayerHUD HUD;
        
        [HideInInspector]
        public PlayerController movement;
        [HideInInspector] 
        public MikuniBucket mikuniBucketController;

        public Vector3 cauldronPosition;

        public Image compassArrow;
        public RectMask2D pokeOverlayMask;
        public Image pokeCooldown;
        public float pokeMaskFull;
        public Slider progressBar;
        public GameObject mikuniCounterObject;
        public Slider redScoreDisplay;
        public Slider blueScoreDisplay;
        public GameObject weaponOverlay;
        public TextMeshProUGUI weaponCooldownText;
        public GameObject utensilTexture;
        public List<GameObject> toHide;

        public bool CompassVisible
        {
            set
            {
                compassArrow.gameObject.SetActive(value);
                _enableCompass = value;
            }
            get => _enableCompass;
        }

        private bool _enableCompass = true;
        private RectMask2D _weaponOverlayMask;
        private Image _utensilImage;
        private int maxScore = 40;

        public string CoolDownText
        {
            set => weaponCooldownText.text = value;
        }

        public float CooldownOverlayProgress
        {
            set => _weaponOverlayMask.padding = new Vector4(0, 0, 0, value * 111f);
        }

        private void OnEnable()
        {
            _weaponOverlayMask = weaponOverlay.GetComponent<RectMask2D>();
            _utensilImage = utensilTexture.GetComponent<Image>();
            if (HUD == null)
            {
                HUD = this;
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = false;
            }
            SetMaxScore(GameManager.Instance.TotalMikuniToCapture);
        }

        public void SetMaxScore(int score)
        {
            maxScore = score;
            redScoreDisplay.maxValue = maxScore;
            blueScoreDisplay.maxValue = maxScore;
        }

        void LateUpdate()
        {
            if (movement == null || mikuniBucketController == null) return;
            float ratio = movement.Stamina / movement.maxStamina;
            progressBar.value = ratio;
            mikuniCounterObject.GetComponent<TextMeshProUGUI>().text = $"{mikuniBucketController.MikuniCatched}";
            if (GameManager.Instance != null)
            {
                uint[] scores = GameManager.Instance.scores;
                redScoreDisplay.value = scores[0];
                blueScoreDisplay.value = scores[1];
            }
            _utensilImage.sprite = movement.ustencil.icon;

            if (_enableCompass)
            {
                Vector3 targetPos = Camera.main.transform.InverseTransformPoint(cauldronPosition);
                float uiAngle = -Mathf.Atan2(targetPos.x, targetPos.y) * Mathf.Rad2Deg;
                compassArrow.transform.eulerAngles = new Vector3(0, 0, uiAngle);

                float sqrDst = (cauldronPosition - movement.transform.position).sqrMagnitude;
                if (sqrDst < 4900)
                {
                    Color current = compassArrow.color;
                    current.a = 0.8f * (sqrDst / 4900);
                    compassArrow.color = current;
                }
            }

            float pokeProgress = movement.pokeCooldown * 1f / PlayerController.POKE_COOLDOWN_MAX;
            pokeOverlayMask.padding = new Vector4(0, 0, 0, pokeProgress * pokeMaskFull);
            float col = movement.pokeCooldown != 0 ? 0.75f : 1f;
            pokeCooldown.transform.localScale = Vector3.one * (0.7f + (1 - pokeProgress) * 0.3f);
            pokeCooldown.color = new Color(col, col, col, col);
        }

        public void ShowTimesUp()
        {
            TimerManager.TimerInstance.ShowTimesup();
            foreach (GameObject o in toHide)
            {
                o.SetActive(false);
            }
        }
    }
}
