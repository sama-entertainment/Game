﻿using network;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace menus
{
    public class CreditsMenu : MonoBehaviour
    {
        public int MenuSceneId;

        public void OnQuitButtonPressed()
        {
            if(PhotonRoom.CurrentRoom != null)
                Destroy(PhotonRoom.CurrentRoom.gameObject);
            SceneManager.LoadSceneAsync(MenuSceneId);
        }
    }
}