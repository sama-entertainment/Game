using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace menus
{
    public class PlayerListPopup : MonoBehaviour
    {

        public LobbyPlayerGridElement[] gridElements;
        public Text statusText;
        public GameObject startButton;
        public GameObject popupObject;
        public Button[] buttonsToDisable;
        public bool allPlayersReady;
    
        private bool _enabled;
        private CanvasGroup _canvasGroup;
        private EaseBackAnimation _popupAnimation;
        private EaseBackAnimation _startButtonAnimation;
        public CanvasGroup dimBG;

        public void Open()
        {
            _enabled = true;
            _canvasGroup.interactable = true;
            foreach (Button button in buttonsToDisable)
            {
                button.interactable = false;
            }
        }

        // Start is called before the first frame update
        void Start()
        { 
            _enabled = false;
            _canvasGroup = GetComponent<CanvasGroup>();
            _canvasGroup.interactable = true;
            _popupAnimation = popupObject.GetComponent<EaseBackAnimation>();
            _startButtonAnimation = startButton.GetComponent<EaseBackAnimation>();
        }

        private void LateUpdate()
        {
            _popupAnimation.enable = _enabled;
            statusText.gameObject.SetActive(!(allPlayersReady && PhotonNetwork.IsMasterClient) && _enabled);
            _startButtonAnimation.enable = allPlayersReady && PhotonNetwork.IsMasterClient;
            statusText.text = "En attente " + (!allPlayersReady ? "des joueurs" : "du maître de la partie") + "...";
        }

        void FixedUpdate()
        {
            if (_enabled)
            {
                if (dimBG.alpha < 1f)
                {
                    dimBG.alpha += 0.04f;
                }
            }
            else
            {
                if (dimBG.alpha > 0)
                {
                    dimBG.alpha -= 0.04f;
                }
            }
        }

        public void Close()
        {
            _enabled = false;
            _canvasGroup.interactable = false;
            foreach (Button button in buttonsToDisable)
            {
                button.interactable = true;
            }
        }
    }
}
