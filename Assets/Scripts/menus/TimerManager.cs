﻿using System;
using network;
using Photon.Pun;
using sound;
using TMPro;
using UnityEngine;

namespace menus
{
    public class TimerManager : MonoBehaviour
    {
        public static TimerManager TimerInstance;
        public const int STATE_UNSTARTED = 0;
        public const int STATE_TIMER_COUNT_DOWN = 1;
        public const int STATE_TIMER_COUNT_UP = 2;
        public const int STATE_PAUSED = 3;
        
        private PhotonView _view;
        private int _timerState = STATE_UNSTARTED;
        private long _timestamp;
        private bool playedTimesup;
        public event Action onTimerEnd;

        public GameObject timerText;
        public GameObject titleText;
        public AudioSource backgroundAudio;
        public AudioSource gameEndSound;
        private TextMeshProUGUI _ugui;
        private TextMeshProUGUI _title;

        private void Start()
        {
            AudioManager.CurrentManager.StopBgMusic();
            TimerInstance = this;
            _view = GetComponent<PhotonView>();
            _ugui = timerText.GetComponent<TextMeshProUGUI>();
            _title = titleText.GetComponent<TextMeshProUGUI>();
            if (_ugui == null) throw new Exception("Could not find TextMeshPro component on timerText");
        }

        void LateUpdate()
        {
            if (_timerState == STATE_UNSTARTED)
            {
                _ugui.text = "";
                return;
            }

            TimeSpan elapsed;
            if (_timerState == STATE_TIMER_COUNT_UP)
            {
                elapsed = DateTime.Now - new DateTime(_timestamp * TimeSpan.TicksPerMillisecond);
            }
            else if (_timerState == STATE_TIMER_COUNT_DOWN)
            {
                elapsed = new DateTime(_timestamp * TimeSpan.TicksPerMillisecond) - DateTime.Now;
                if (elapsed.TotalMilliseconds <= 0)
                {
                    Reset();
                    onTimerEnd?.Invoke();
                } else if (elapsed.Minutes == 0)
                {
                    if (elapsed.Seconds == 30)
                    {
                        _title.text = elapsed.Seconds + "";
                        _title.GetComponent<RectTransform>().localScale *= 1.003f;
                    } else if (elapsed.Seconds == 0)
                    {
                        ShowTimesup();
                    }
                    else if (elapsed.Seconds <= 5 && elapsed.Seconds > 0)
                    {
                        _title.text = elapsed.Seconds + "";
                        _title.GetComponent<RectTransform>().localScale *= 1.005f;
                    }
                    else
                    {
                        _title.GetComponent<RectTransform>().localScale = Vector3.one;
                        _title.text = "";
                    }
                }
                if (backgroundAudio.time >= 3 * 60 && elapsed.Minutes > 0 && !playedTimesup)
                {
                    Debug.Log("Restarting backgroundAudio");
                    backgroundAudio = Instantiate(backgroundAudio);
                    backgroundAudio.Play();
                }
            }

            _ugui.text = elapsed.Minutes.ToString().PadLeft(2, '0') + ":" +
                         elapsed.Seconds.ToString().PadLeft(2, '0');
        }

        public void StartStopwatch()
        {
            if (PhotonNetwork.InRoom && !PhotonNetwork.IsMasterClient) return;
            long startTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            _view.RPC("RPC_BroadcastTimerUpdate", RpcTarget.AllBuffered, 
                STATE_TIMER_COUNT_UP, startTime);
        }

        public void StartCountDown(DateTime endDate)
        {
            if (PhotonNetwork.InRoom && !PhotonNetwork.IsMasterClient) return;
            long endTime = endDate.Ticks / TimeSpan.TicksPerMillisecond;
            _view.RPC("RPC_BroadcastTimerUpdate", RpcTarget.AllBuffered,
                STATE_TIMER_COUNT_DOWN, endTime);
        }

        public void PauseTimer()
        {
            if (PhotonNetwork.InRoom && !PhotonNetwork.IsMasterClient) return;
            _view.RPC("RPC_BroadcastTimerUpdate", RpcTarget.AllBuffered,
                STATE_PAUSED, _timestamp);
        }

        public void Reset()
        {
            if (PhotonNetwork.InRoom && !PhotonNetwork.IsMasterClient) return;
            _view.RPC("RPC_BroadcastTimerUpdate", RpcTarget.AllBuffered,
                STATE_UNSTARTED, (long)-1);
        }

        public void ShowTimesup()
        {
            backgroundAudio.Stop();
            _title.text = "Terminé !";
            _title.GetComponent<RectTransform>().localScale = Vector3.one;
            if (!playedTimesup)
            {
                gameEndSound.Play();
                playedTimesup = true;
            }
        }

        [PunRPC]
        public void RPC_BroadcastTimerUpdate(int timerState, long timestamp)
        {
            if (_timerState == STATE_UNSTARTED || _timerState == STATE_PAUSED)
            {
                if (timerState != STATE_UNSTARTED && timerState != STATE_PAUSED)
                {
                    playedTimesup = false;
                    backgroundAudio.Play();
                }
            }
            else if(timerState == STATE_UNSTARTED || timerState == STATE_PAUSED)
            {
                backgroundAudio.Stop();
            }
            this._timerState = timerState;
            this._timestamp = timestamp;
        }
    }
}