﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace menus
{
    [RequireComponent(typeof(Slider))]
    public class MaskSlider : MonoBehaviour
    {
        private Slider _slider;
        public RectMask2D mask;
        public float maxRectPadding;

        private void Awake()
        {
            _slider = GetComponent<Slider>();
        }
        
        private void LateUpdate()
        {
            Vector4 newPadding = mask.padding;
            newPadding.z = (1 - _slider.normalizedValue) * maxRectPadding;
            mask.padding = newPadding;
        }
    }
}