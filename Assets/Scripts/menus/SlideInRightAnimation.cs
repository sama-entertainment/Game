using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class SlideInRightAnimation : MonoBehaviour
{
    public static bool AllFinished;
    public bool enable;
    [Range(0.001f, 1f)] public float speed;
    
    private float _animProgress;
    private float _updatedProgress;
    private const int framerateDiv = 1;
    private int _timer;
    private float _baseX;

    private RectTransform _rectTransform;
    // Start is called before the first frame update
    void Start()
    {
        _animProgress = enable ? 1f : 0;
        _timer = 0;
        _rectTransform = (RectTransform) transform;
        _baseX = _rectTransform.anchoredPosition.x;
    }

    void FixedUpdate()
    {
        if (_timer >= framerateDiv) _timer = 0;
        if (_timer == 0)
        {
            if (enable)
            {
                if (_animProgress < 1f)
                {
                    // _animProgress += 1f - Mathf.Exp(-5f * speed);
                    _animProgress += speed / 5;
                    if (AllFinished)
                    {
                        AllFinished = false;
                    }
                }
                else
                {
                    if (!AllFinished)
                    {
                        AllFinished = true;
                    }
                }
            }
            else
            {
                if (_animProgress > 0)
                {
                    // _animProgress -= 1f - Mathf.Exp(-5f * speed);
                    _animProgress -= speed / 5;
                    if (AllFinished)
                    {
                        AllFinished = false;
                    }
                }
                else
                {
                    if (!AllFinished)
                    {
                        AllFinished = true;
                    }
                }
            }
        }

        _timer++;

        _animProgress = Mathf.Clamp01(_animProgress);

        _updatedProgress = EaseOutCubic(_animProgress);
    }

    private void Update()
    {
        Vector2 position = _rectTransform.anchoredPosition;
        position.x = Mathf.Lerp(_baseX - _rectTransform.rect.width, _baseX, _updatedProgress);
        _rectTransform.anchoredPosition = position;
    }

    private float EaseOutCubic(float x) => 1 - Mathf.Pow(1 - x, 3);
}
