using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace menus
{
    public class WeaponStatsDisplayer : MonoBehaviour
    {
        public static int speed;
        public static int capacity;
        public static int strength;
        public static int range;
        public static string Name = "Arme";

        public TextMeshProUGUI nametag;
        public MKSlider speedSlider;
        public MKSlider capacitySlider;
        public MKSlider strengthSlider;
        public MKSlider rangeSlider;

        private void FixedUpdate()
        {
            nametag.text = Name;
            speedSlider.value = speed;
            capacitySlider.value = capacity;
            strengthSlider.value = strength;
            rangeSlider.value = range;
        }
    }
}
