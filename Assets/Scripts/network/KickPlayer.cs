using System;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;

public class NewBehaviourScript : MonoBehaviourPunCallbacks
{
    public GameObject player1;
    private Toggle _player1;
    private Text _player1Pseudo;
    public GameObject player2;
    private Toggle _player2;
    private Text _player2Pseudo;
    public GameObject player3;
    private Toggle _player3;
    private Text _player3Pseudo;

    void Start()
    {
        _player1 = player1.GetComponent<Toggle>();
        _player1 = player2.GetComponent<Toggle>();
        _player1 = player3.GetComponent<Toggle>();
    }

    private void kickPlayers()
    {
        Toggle[] players = {_player1, _player2, _player3};
        string[] pseudos = {_player1Pseudo.text, _player2Pseudo.text, _player3Pseudo.text};
        for (int i = 0; i < 3; i++)
        {
            if (players[i].isOn)
            {
                Player[] listPlayers = PhotonNetwork.PlayerList;
                for (int j = 0; j < listPlayers.Length; j++)
                {
                    if (pseudos[i] == listPlayers[j].NickName)
                    {
                        PhotonNetwork.CloseConnection(listPlayers[j]);
                        Debug.Log(listPlayers[j].NickName + " has been kicked by " + PhotonNetwork.MasterClient.NickName);
                    } 
                }
            }
        }
    }

    public void Kick()
    {
        kickPlayers();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
