using System;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;

namespace network
{
    public class PhotonNewRoom : MonoBehaviourPunCallbacks
    {
        public static PhotonNewRoom Lobby { get; private set; }
        private System.Random _rd = new System.Random();
        private int _createRoomAttempts;
        public Text roomName;
        public GameObject privateRoomToggle;
        private Toggle _privateRoomToggleHandle;
        public GameObject password;
        private Text _password;
        public GameObject visibleRoomToggle;
        private Toggle _visibleRoomToggleHandle;
        public GameObject createButton;
        private Button _createButtonHandle;
        public GameObject nbMikunis;
        private Slider _nbMikunisHandle;
        public GameObject time3;
        private Toggle _time3;
        public GameObject time6;
        private Toggle _time6;
        public GameObject time9;
        private Toggle _time9;

        private void Awake()
        {
            Lobby = this;
            _createRoomAttempts = 3;
        }

        void Start()
        {
            _createButtonHandle = createButton.GetComponent<Button>();
            _createButtonHandle.interactable = false;
            _privateRoomToggleHandle = privateRoomToggle.GetComponent<Toggle>();
            _visibleRoomToggleHandle = visibleRoomToggle.GetComponent<Toggle>();
            _password = password.GetComponent<Text>();
            _nbMikunisHandle = nbMikunis.GetComponent<Slider>();
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("Successfully connected to master");
            PhotonNetwork.AutomaticallySyncScene = true;
            _createButtonHandle.interactable = true;
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("Failed to create room (" + returnCode + ": " + message + ")");
            if (_createRoomAttempts > 0)
            {
                _createRoomAttempts--;
                CreateRoom();
            }
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("Cannot join the room!");
        }

        private bool isValidName(string roomName)
        {
            for (int i = 0; i < roomName.Length; i++)
            {
                if (roomName[i] == '#')
                    {
                        Debug.Log("Invalid character '#'!");
                        return false;
                    }
                if (roomName[i] == ':')
                {
                    Debug.Log("Invalid character ':'!");
                    return false;
                }
            }
            return true;
        }        

        void CreateRoom()
        {
            byte[] roomId = new byte[4];
            _rd.NextBytes(roomId);
            string roomCode = "#" + BitConverter.ToString(roomId).Replace("-", "").ToLower();
            RoomOptions opts = new RoomOptions() {IsVisible = _visibleRoomToggleHandle.isOn, IsOpen = true, MaxPlayers = 4};
            PhotonNetwork.CreateRoom(roomCode, opts);
            Debug.Log("Room<" + roomCode + "> created with settings={IsVisible = " + _visibleRoomToggleHandle.isOn + ", IsOpen = true, MaxPlayers = 4}");
            Hashtable roomCustomProperties = new Hashtable();
            roomCustomProperties["Name"] = roomName.text;
            roomCustomProperties["Code"] = roomCode;
            if (_privateRoomToggleHandle.isOn)
                roomCustomProperties["Password"] = _password.text;
            else
                roomCustomProperties["Password"] = null;
            roomCustomProperties["Mikunis"] = (int) _nbMikunisHandle.value;
            if (_time3.isOn)
                roomCustomProperties["Time"] = 3;
            if (_time6.isOn)
                roomCustomProperties["Time"] = 6;
            if (_time9.isOn)
                roomCustomProperties["Time"] = 9;
            PhotonNetwork.CurrentRoom.SetCustomProperties(roomCustomProperties);
        }

        public void OnCreateButtonClicked()
        {
            if (isValidName(roomName.text))
                CreateRoom();
        }


        // Update is called once per frame
        void Update()
        {
            
        }
    }
}
