using System;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;

namespace network
{
    public class PhotonJoinRoom : MonoBehaviourPunCallbacks
    {
        public static PhotonJoinRoom Lobby { get; private set; }
        public GameObject joinButton;
        private Button _joinButtonHandle;
        public GameObject roomCode;
        private Text _roomCode;
        public GameObject password;
        private Text _password;

        private void Awake()
        {
            Lobby = this;
        }

        void Start()
        {
            _joinButtonHandle = joinButton.GetComponent<Button>();
            _joinButtonHandle.interactable = false;
            _roomCode = roomCode.GetComponent<Text>();
            _password = password.GetComponent<Text>();
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("Successfully connected to master");
            PhotonNetwork.AutomaticallySyncScene = true;
            _joinButtonHandle.interactable = true;
        }

        void JoinRoom(string roomCode, string password)
        {
            Debug.Log("Joining room...");
            PhotonNetwork.JoinRoom("#" + roomCode);
            Room room = PhotonNetwork.CurrentRoom;
            if (room.CustomProperties["Password"] != null & room.CustomProperties["Password"].ToString() != password)
            {
                Debug.Log("Wrong password");
                PhotonNetwork.LeaveRoom();
            }
        }

        public void OnJoinButtonClicked()
        {
            JoinRoom(_roomCode.text, _password.text);
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("Room not found!");
        }

        // Update is called once per frame
        void Update()
        {
            
        }
    }
}
