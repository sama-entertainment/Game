using System;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;

public class PhotonRoomSettings : MonoBehaviourPunCallbacks
{
    private Room current = PhotonNetwork.CurrentRoom;
    public GameObject roomName;
    private Text _roomName;
    public GameObject privateRoomToggle;
    private Toggle _privateRoomToggleHandle;
    public GameObject visibleRoomToggle;
    private Toggle _visibleRoomToggleHandle;
    public GameObject applyButton;
    private Button _applyButtonHandle;
    public GameObject password;
    private Text _password;
    public GameObject nbMikunis;
    private Slider _nbMikunis;
    public GameObject time3;
    private Toggle _time3;
    public GameObject time6;
    private Toggle _time6;
    public GameObject time9;
    private Toggle _time9;
    
    void Start()
    {
        _applyButtonHandle = applyButton.GetComponent<Button>();
        _applyButtonHandle.interactable = true;
        _privateRoomToggleHandle = privateRoomToggle.GetComponent<Toggle>();
        _privateRoomToggleHandle.interactable = true;
        _visibleRoomToggleHandle = visibleRoomToggle.GetComponent<Toggle>();
        _visibleRoomToggleHandle.interactable = true;
        _nbMikunis = nbMikunis.GetComponent<Slider>();
        _roomName = roomName.GetComponent<Text>();
        _password = password.GetComponent<Text>();
    }

    private bool isValidName(string roomName)
        {
            for (int i = 0; i < roomName.Length; i++)
            {
                if (roomName[i] == '#')
                    {
                        Debug.Log("Invalid character '#'!");
                        return false;
                    }
                if (roomName[i] == ':')
                {
                    Debug.Log("Invalid character ':'!");
                    return false;
                }
            }
            return true;
        }      

    void applyChanges()
    {
        current.IsVisible = _visibleRoomToggleHandle.isOn;
        current.IsOpen = !_privateRoomToggleHandle.isOn;
        Hashtable newCustomProperties = new Hashtable();
        newCustomProperties["Name"] = _roomName.text;
        newCustomProperties["Code"] = current.CustomProperties["Code"].ToString();
        if (_privateRoomToggleHandle.isOn)
            newCustomProperties["Password"] = _password.text;
        else
            newCustomProperties["Password"] = null;
        newCustomProperties["Mikunis"] = (int) _nbMikunis.value;
        if (_time3.isOn)
            newCustomProperties["Time"] = 3;
        if (_time6.isOn)
            newCustomProperties["Time"] = 6;
        if (_time9.isOn)
            newCustomProperties["Time"] = 9;
        current.SetCustomProperties(newCustomProperties);
    }

    public void OnApplyButtonClicked()
    {
        if (isValidName(_roomName.text))
            applyChanges();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
