using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using gameplay;
using menus;
using Photon.Pun;
using player;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace network.controllers
{
    public class PhotonPlayer : MonoBehaviour
    {
        public PhotonView _view;
        private GameObject _avatar;
        public int teamId = -1;
    
        void Start()
        {
            _view = GetComponent<PhotonView>();
            if (_view.IsMine)
            {
                _view.RPC("RPC_GetTeam", RpcTarget.MasterClient);
            }
        }

        void Update()
        {
            if (!_view.IsMine || _avatar != null || teamId == -1 || !SceneManager.GetActiveScene().isLoaded) return;
            Transform[] spawnPoints =
                teamId == 0 ? GameManager.Instance.blueSpawnPoints : GameManager.Instance.redSpawnPoints;
            int spawnPointIdx = Random.Range(0, spawnPoints.Length);
            Transform spawnPoint = spawnPoints[spawnPointIdx];
            PlayerInfo.PInfo.teamId = teamId;
            _avatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerAvatar"),
                spawnPoint.position, spawnPoint.rotation, 0);
            _avatar.GetComponent<PlayerController>().Player = this;
            GameManager.Instance._view.RPC("RPC_NotifyPlayerSpawned", RpcTarget.MasterClient,
                _view.Controller.ActorNumber, _view.ViewID);
            int mikunisToCapture = (int)PhotonNetwork.CurrentRoom.CustomProperties["Mikunis"];
            if (mikunisToCapture < 1) mikunisToCapture = 1;
            PlayerHUD.HUD.SetMaxScore(mikunisToCapture);
            GameManager.Instance.TotalMikuniToCapture = mikunisToCapture;
        }

        [PunRPC]
        void RPC_StartGame()
        {
            if (_avatar != null)
            {
                _avatar.GetComponent<PlayerController>().UnFreeze();
            }
        }

        [PunRPC]
        void RPC_GetTeam()
        {
            teamId = GameManager.Instance.nextPlayersTeam;
            GameManager.Instance.UpdateTeam();
            _view.RPC("RPC_UpdateTeam", RpcTarget.OthersBuffered, teamId);
        }

        [PunRPC]
        void RPC_UpdateTeam(int which)
        {
            teamId = which;
        }
        
        [PunRPC]
        void RPC_PushScore(int team, int score)
        {
            if (score < 0) return;
            uint newScore = GameManager.Instance.scores[team] + (uint)score;
            GameManager.Instance.scores[team] = newScore;
            _view.RPC("RPC_UpdateScore", RpcTarget.OthersBuffered, 
                team, (int)newScore);
            if (newScore >= GameManager.Instance.TotalMikuniToCapture)
            {
                GameManager.Instance.StopGame();
            }
        }

        [PunRPC]
        void RPC_UpdateScore(int team, int newScore)
        {
            if (newScore < 0) return;
            GameManager.Instance.scores[team] = (uint)newScore;
        }

        [PunRPC]
        void RPC_StopGame()
        {
            if (_avatar != null)
            {
                _avatar.GetComponent<PlayerController>()?.Freeze();
                PlayerHUD.HUD.ShowTimesUp();
            }
        }

        public void DestroyMikuni(GameObject mikuni)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.Destroy(mikuni);
            }
            else
            {
                _view.RPC("RPC_DestroyMikuni", RpcTarget.MasterClient, 
                    mikuni.GetComponent<PhotonView>().ViewID);
            }
        }

        [PunRPC]
        void RPC_DestroyMikuni(int viewID)
        {
            PhotonNetwork.Destroy(PhotonNetwork.GetPhotonView(viewID));
        }

        [PunRPC]
        void RPC_NotifyMikuniDropped(int viewID)
        {
            GameObject obj = PhotonNetwork.GetPhotonView(viewID).gameObject;
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.detectCollisions = false;
        }

        [PunRPC]
        void RPC_DropMikunis(int[] viewIds, Vector3 spawnPoint)
        {
            for(int i = 0; i < viewIds.Length; i++)
            {
                int viewId = viewIds[i];
                if (viewId == -1) continue;
                GameObject obj = PhotonNetwork.GetPhotonView(viewId).gameObject;
                Vector3 position = spawnPoint + Random.Range(-1f, 1f) * Vector3.right 
                                              + Random.Range(-1f, 1f) * Vector3.forward;
                MikuniBank.Instance.SetupMikuni(obj, position, Random.rotation);
            }
        }
    }
}
