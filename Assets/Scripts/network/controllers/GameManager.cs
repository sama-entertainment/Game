﻿using System;
using System.Collections;
using System.Collections.Generic;
using menus;
using Photon.Pun;
using UnityEngine;

namespace network.controllers
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        public Transform[] redSpawnPoints;
        public Transform[] blueSpawnPoints;
        [HideInInspector]
        public int nextPlayersTeam = 0;
        [HideInInspector]
        public uint[] scores = new uint[2];
        private Dictionary<int, int> _readyPlayers = new Dictionary<int, int>();

        public PhotonView _view;
        public int TotalMikuniToCapture = 40;
        public int EndGameSceneId = 3;

        private void OnEnable()
        {
            if(Instance == null) Instance = this;
        }

        public void UpdateTeam()
        {
            nextPlayersTeam = (nextPlayersTeam + 1) % 2;
        }
            
        [PunRPC]
        void RPC_NotifyPlayerSpawned(int actor, int viewID)
        {
            if (_readyPlayers.ContainsKey(actor)) return;
            _readyPlayers.Add(actor, viewID);
            Debug.Log("Waiting for all players to spawn (" + _readyPlayers.Count 
                      + "/" + PhotonRoom.CurrentRoom.PlayerCount + ")");
            if (_readyPlayers.Count == PhotonRoom.CurrentRoom.PlayerCount)
            {
                // START GAME
                Debug.Log("Starting Game");
                TimerManager.TimerInstance.onTimerEnd += StopGame;
                foreach (var pair in _readyPlayers)
                {
                    PhotonNetwork.GetPhotonView(pair.Value).RPC("RPC_StartGame", RpcTarget.AllBuffered);
                }
                int secs = (int)PhotonNetwork.CurrentRoom.CustomProperties["Time"];
                if (secs < 0) secs = 0;
                TimerManager.TimerInstance.StartCountDown(DateTime.Now + TimeSpan.FromSeconds(secs + 1));
            }
        }
        
        public void StopGame()
        {
            if (!PhotonNetwork.IsMasterClient) return;
            foreach (int viewId in _readyPlayers.Values)
            {
                PhotonNetwork.GetPhotonView(viewId).RPC("RPC_StopGame", RpcTarget.AllBuffered);
                Debug.Log("Detected player to be frozen: " + viewId);
            }
            StartCoroutine(EndGameCoroutine());
        }
        
        IEnumerator EndGameCoroutine()
        {
            yield return new WaitForSeconds(5f);
            PhotonNetwork.LoadLevel(EndGameSceneId);
        }
    }
}