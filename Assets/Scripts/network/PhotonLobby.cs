﻿using System;
using System.Security.Policy;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using ExitGames.Client.Photon;

namespace network
{
    public class PhotonLobby : MonoBehaviourPunCallbacks
    {
        public static PhotonLobby Lobby { get; private set; }
        public int RoomBrowserSceneID;
        public Button createButton;
        public Button joinButton;
        private System.Random _rd = new System.Random();
        private int _createRoomAttempts;
        public GameObject gameIdInput;
            
        private void Awake()
        {
            Lobby = this;
            _createRoomAttempts = 3;
        }

        void Start()
        {
            if (PhotonNetwork.IsConnectedAndReady) return;
            PhotonNetwork.ConnectUsingSettings(); // Connects to the PUN master server
            createButton.interactable = false;
            createButton.GetComponentInChildren<TextMeshProUGUI>().text = "Connexion...";
            joinButton.interactable = false;
            joinButton.GetComponentInChildren<TextMeshProUGUI>().text = "Connexion...";
        }
        
        public override void OnConnectedToMaster()
        {
            createButton.GetComponentInChildren<TextMeshProUGUI>().text = "Créer une salle";
            joinButton.GetComponentInChildren<TextMeshProUGUI>().text = "Rejoindre une salle";
            Debug.Log("Successfully connected to master");
            PhotonNetwork.AutomaticallySyncScene = true;
            createButton.interactable = true;
            joinButton.interactable = true;
            try
            { 
                PhotonNetwork.LocalPlayer.NickName = PlayerPrefs.GetString("OnlineUsername", "Player");
            }
            catch (Exception e)
            {
                Debug.LogWarning("Failed to set OnlineUsername: " + e);
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            Debug.LogError("Disconnected from Photon: " + cause);
            if (createButton != null && joinButton != null)
            {
                createButton.interactable = false;
                createButton.GetComponentInChildren<TextMeshProUGUI>().text = "Erreur: " + cause;
                joinButton.interactable = false;
                joinButton.GetComponentInChildren<TextMeshProUGUI>().text = "Erreur: " + cause;
            }
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("Failed to create room (" + returnCode + ": " + message + ")");
            if (_createRoomAttempts > 0)
            {
                _createRoomAttempts--;
                CreateRoom();
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("No random room found!");
            CreateRoom();
        }

        public override void OnJoinedRoom()
        {
            Hashtable props = new Hashtable();
            props["JoinDate"] = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }

        void CreateRoom()
        {
            byte[] roomId = new byte[4];
            _rd.NextBytes(roomId);
            string roomCode = "mk:room:" + BitConverter.ToString(roomId).Replace("-", "").ToUpper();
            RoomOptions opts = new RoomOptions()
            {
                IsVisible = true, IsOpen = true, MaxPlayers = 4,
                CustomRoomProperties = new Hashtable() // Default Room configuration
                {
                    { "Name", PhotonNetwork.LocalPlayer.NickName + "'s room" },
                    { "Mikunis", 40 },
                    { "Time", 180 },
                    { "Password", "" },
                    { "Master", PhotonNetwork.LocalPlayer.NickName }
                },
                CustomRoomPropertiesForLobby = new[] { "Name", "Mikunis", "Time", "Password", "Master" }
            };
            PhotonNetwork.CreateRoom(roomCode, opts);
            Debug.Log("Room<" + roomCode + "> created with default settings");
        }

        public void JoinNamedRoom()
        {
            string name = gameIdInput.GetComponent<TMP_InputField>().text
                .ToUpper().Replace("mk:room:", "");
            PhotonNetwork.JoinRoom("mk:room:" + name);
        }

        public void OnJoinButtonClicked()
        { 
            SceneManager.LoadSceneAsync(RoomBrowserSceneID);
        }

        public void OnCreateButtonClicked()
        {
            Debug.Log("Creating room...");
            CreateRoom();
        }

        public void JoinRandomRoom()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }
    }
}