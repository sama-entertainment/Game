﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace network
{
    public class RoomListManager : MonoBehaviourPunCallbacks
    {
        public int fallbackScene;
        private readonly Dictionary<string, RoomInfo> _cachedRoomList = new Dictionary<string, RoomInfo>();
        public Dictionary<string, RoomInfo> RoomList => _cachedRoomList;

        private void Start()
        {
            PhotonNetwork.JoinLobby(TypedLobby.Default);
        }

        private void UpdateCachedRoomList(List<RoomInfo> roomList)
        {
            for (int i = 0; i < roomList.Count; i++)
            {
                RoomInfo info = roomList[i];
                if (info.RemovedFromList)
                {
                    _cachedRoomList.Remove(info.Name);
                }
                else
                {
                    _cachedRoomList[info.Name] = info;
                    Debug.Log("New room detected: " + info.CustomProperties["Name"]);
                }
            }
        }

        public override void OnJoinedLobby()
        {
            _cachedRoomList.Clear();
            Debug.Log("Successfully joined lobby");
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            UpdateCachedRoomList(roomList);
        }

        public override void OnLeftLobby()
        {
            _cachedRoomList.Clear();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            _cachedRoomList.Clear();
        }

        public void LeaveLobby()
        {
            PhotonNetwork.LeaveLobby();
            if(fallbackScene == 0 && PhotonRoom.CurrentRoom != null) // Main Scene
                Destroy(PhotonRoom.CurrentRoom.gameObject);
            SceneManager.LoadSceneAsync(fallbackScene);
        }
    }
}