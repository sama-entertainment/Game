﻿using UnityEngine;

namespace player
{
    /**
     * Placeholder class for accessing Cook's character transforms
     */
    public class Basket : MonoBehaviour
    {
        public ParticleSystem forbiddenSign;
        public ParticleSystem pokeAnimation;
        public Renderer pants;
    }
}