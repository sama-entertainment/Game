using System.Collections.Generic;
using mikunis;
using Photon.Pun;
using UnityEngine;

namespace player
{
    public class MikuniViewer : MonoBehaviour
    {
        public float scale = 0.005f;
        public GameObject particles;
        public List<Transform> positions;
        private readonly List<Mikuni> _mikunis = new List<Mikuni>();
        private ParticleSystem _ps;

        private void Awake()
        {
            if(particles != null) _ps = particles.GetComponent<ParticleSystem>();
        }

        public void DisplayMikuni(Mikuni mikuni, bool remote)
        {
            if (mikuni == null) return;
            mikuni.SetCaptured(true);
            if (!remote)
            {
                mikuni.transform.localScale = Vector3.one * scale;
            }
            if(_ps != null) _ps.Play();
            _mikunis.Add(mikuni);
            Rerender();
        }

        public void DisplayMikunis(List<Mikuni> mikunis, bool remote)
        {
            if (mikunis.Count == 0) return;
            if(_ps != null) _ps.Play();
            _mikunis.AddRange(mikunis);
            if (!remote)
            {
                foreach (Mikuni mikuni in mikunis)
                {
                    mikuni.SetCaptured(true);
                }
            }
            Rerender();
        }

        public void ReleaseMikuni(Mikuni mikuni)
        {
            mikuni.transform.parent = null;
            mikuni.gameObject.SetActive(true);
            mikuni.SetCaptured(false);
            mikuni.transform.localScale = Vector3.one;
            _mikunis.Remove(mikuni);
            Rerender();
        }

        public void Rerender()
        {
            for (int i = 0; i < positions.Count; i++)
            {
                if (i >= _mikunis.Count)
                {
                    for (int j = 0; j < positions[i].childCount; j++)
                    {
                        positions[i].GetChild(j).parent = null;
                    }
                }
                else
                {
                    Mikuni mikuni = _mikunis[i];
                    if(mikuni == null || mikuni.gameObject == null) continue;
                    mikuni.gameObject.SetActive(true);
                    var tr = mikuni.transform;
                    tr.parent = positions[i];
                    tr.localScale = Vector3.one * scale;
                    tr.localPosition = Vector3.zero;
                }
            }

            if (_mikunis.Count > positions.Count)
            {
                for (int i = positions.Count; i < _mikunis.Count; i++)
                {
                    Mikuni mikuni = _mikunis[i];
                    if(mikuni == null || mikuni.gameObject == null) continue;
                    mikuni.gameObject.SetActive(false);
                }
            }
        }

        public void Clear(Transform newParent)
        {
            foreach (Transform tr in positions)
            {
                for (int j = 0; j < tr.childCount; j++)
                {
                    Transform child = tr.GetChild(j);
                    child.parent = newParent;
                    Mikuni mikuni = child.GetComponent<Mikuni>();
                    if (mikuni != null)
                    {
                        mikuni.gameObject.SetActive(true);
                        mikuni.transform.localScale = Vector3.one;
                        mikuni.SetCaptured(false);
                    }
                }
            }

            _mikunis.Clear();
        }

        public void Destroy()
        {
            _mikunis.Clear();
        }
    }
}
