using System;
using System.Collections.Generic;
using gameplay;
using menus;
using mikunis;
using Photon.Pun;
using UnityEngine;
using ustensils;

namespace player
{
    public class MikuniBucket : MonoBehaviour
    {
        private PhotonView _view;
        private List<Mikuni> _caughtMikunis;
        private MikuniViewer _viewer;

        public readonly int Capacity = 12;
    
        public int MikuniCatched => _caughtMikunis.Count;
        private GameObject _cauldron;
        private PlayerController Controller;
        private float _cooldown;
        private List<PlayerController> _nearbyPlayers = new List<PlayerController>();

        void Start()
        {
            _caughtMikunis = new List<Mikuni>();
            Controller = GetComponentInParent<PlayerController>();
            _view = transform.parent.GetComponent<PhotonView>();
            if (_view.IsMine)
            {
                PlayerHUD.HUD.mikuniBucketController = this;
            }
            
            _viewer = Controller.GetComponentInChildren<MikuniViewer>();
            if (_viewer == null) throw new Exception("Missing MikuniViewer script in hierarchy");
            Controller.ustencil.OnTransferMikunis += TransferMikunis;
            Controller.ustencil.OnForbidden += ShowForbiddenSign;
        }
        
        private void FixedUpdate()
        {
            if (!_view.IsMine || Controller.IsFrozen()) return;
            if (Controller._input.releaseMikuni && MikuniCatched > 0)
            {
			    Controller._input.releaseMikuni = false;
                if (_cauldron != null)
                {
                    bool success = _cauldron.GetComponent<MikuniBank>().Put(Controller.Player, _caughtMikunis);
                    if (success)
                    {
                        _viewer.Destroy();
                        _caughtMikunis.Clear();
                        return;
                    }
                }

                ReleaseOne();
            } else if (Controller._input.pokePlayer && Controller.pokeCooldown == 0 && _nearbyPlayers.Count > 0)
            {
                PlayerController toPoke = _nearbyPlayers[0];
                double minDst = (toPoke.transform.position - Controller.transform.position).sqrMagnitude;
                for (int i = 1; i < _nearbyPlayers.Count; i++) // find the nearest player
                {
                    PlayerController candidate = _nearbyPlayers[i];
                    if (candidate == null)
                        return;
                    double dst = (candidate.transform.position - Controller.transform.position).sqrMagnitude;
                    if (dst < minDst)
                    {
                        minDst = dst;
                        toPoke = candidate;
                    }
                }
                Controller.PokePlayer(toPoke.GetPhotonViewId());
            }

            PlayerHUD hud = PlayerHUD.HUD;
            if (_cooldown == 0)
            {
                hud.CooldownOverlayProgress = 1;
                hud.CoolDownText = "";
                if (Controller._input.captureMikuni)
                {
                    if (MikuniCatched >= Capacity)
                    {
                        ShowForbiddenSign();
                    }
                    else
                    {
                        Controller.ustencil.StartCapturingSession();
                    }
                }
                else if (Controller.ustencil.Capturing)
                {
                    if (Controller.ustencil is GrapplingForkUtensil gf 
                        && gf.MomentumCharge < GrapplingForkUtensil.MinimumCharge)
                    {
                        Controller.ustencil.StopCapturingSession();
                    }
                    else
                    {
                        Controller.ustencil.StopCapturingSession();
                        _cooldown = Controller.ustencil.speed;
                    }
                }
            }
            else
            {
                hud.CooldownOverlayProgress = 1 - _cooldown / Controller.ustencil.speed;
                hud.CoolDownText = _cooldown.ToString("0.0") + "s";
                _cooldown -= Time.deltaTime;
                if (_cooldown < 0) _cooldown = 0;
            }


        }

        private void ShowForbiddenSign()
        {
            Basket basket = Controller.GetComponentInChildren<Basket>();
            if(!basket.forbiddenSign.isPlaying) basket.forbiddenSign.Play();
        }

        private void OnTriggerEnter(Collider other)
        {
            GameObject go = other.gameObject;
            if (go.CompareTag("Cauldron"))
            {
                _cauldron = go;
            } else if (go.CompareTag("Player") && Controller != null)
            {
                PlayerController player = go.GetComponent<PlayerController>();
                if (player != null && player.GetPhotonViewId() != Controller.GetPhotonViewId())
                {
                    _nearbyPlayers.Add(player);
                }
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            GameObject go = other.gameObject;
            if (go.CompareTag("Cauldron"))
            {
                _cauldron = null;
            }
            else if (go.CompareTag("Player"))
            {
                PlayerController player = go.GetComponent<PlayerController>();
                if (player != null && _nearbyPlayers.Contains(player))
                {
                    _nearbyPlayers.Remove(player);
                }
            }
        }

        public void TransferMikunis(List<Mikuni> mikunis)
        {
            _caughtMikunis.AddRange(mikunis);
            _viewer.DisplayMikunis(mikunis, false);
            foreach (Mikuni mikuni in mikunis)
            {
                _view.RPC("RPC_CaptureMikuni", RpcTarget.OthersBuffered, mikuni._view.ViewID,
                    false);
            }

            int available = MikuniCatched - Capacity;
            for (int i = 0; i < available; i++)
            {
                ReleaseOne();
            }
        }

        /**
         * Release all caught Mikunis in the world
         */
        public void ReleaseAll()
        {
            _viewer.Clear(null);
            _caughtMikunis.Clear();
        }

        /**
         * Release the first caught Mikuni in the world
         */
        public void ReleaseOne()
        {
            if (_caughtMikunis.Count == 0) return;
            Mikuni mikuni = _caughtMikunis[0];
            _view.RPC("RPC_ReleaseMikuni", RpcTarget.AllBuffered, mikuni._view.ViewID, false);
            _caughtMikunis.Remove(mikuni);
        }

        /**
         * Destroys all caught Mikunis
         */
        public void DestroyAll()
        {
            foreach (Mikuni mikuni in _caughtMikunis)
            {
                PhotonNetwork.Destroy(mikuni.gameObject);
            }
            _viewer.Destroy();
            _caughtMikunis.Clear();
        }

        public void Clear()
        {
            _viewer.Destroy();
            _caughtMikunis.Clear();
        }
    }
}
