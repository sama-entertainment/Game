using System;
using Cinemachine;
using menus;
using mikunis;
using network.controllers;
using Photon.Pun;
using tools;
using UnityEngine;
using UnityEngine.InputSystem;
using ustensils;
using Random = UnityEngine.Random;

namespace player
{
    public class PlayerController : MonoBehaviour
    {
        public const int POKE_COOLDOWN_MAX = 300;
        [HideInInspector]
        public PhotonPlayer Player;
        
        public CharacterController controller;
        public Transform cam;
        public CinemachineVirtualCamera aimCamera;
        public GameObject cameraTarget;
        
        public Animator animator;
      
        public CharacterInput _input;
        public PlayerInput _playerInput;

        private GameObject _tpvCam;
        private float _sensibility;
        private float _speed;
        private float _animationBlend;
        private float _targetRotation = 0.0f;
        private float _rotationVelocity;
        private float _verticalVelocity;
        private float _terminalVelocity = 53.0f;
        private int _animIDSpeed;
        private float _cinemachineTargetYaw;
        private float _cinemachineTargetPitch;
        private int _frozenTicks;
        public int pokeCooldown;
        private const float _threshold = 0.01f;

        [Tooltip("Camera input sensibility")]
        public float CameraSensibility = 2f;
        [Tooltip("Aiming Camera input sensibility")]
        public float AimCameraSensibility = 1f;
        [Tooltip("Player speed when walking")]
        public float BaseSpeed = 7f;
        [Tooltip("Player speed when sprinting")]
        public float SprintSpeed = 13.50f;
        [Tooltip("Base stamina of the Player in seconds")]
        public float maxStamina = 10;
        [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
        public float Gravity = -9.81f;
        [Tooltip("How far in degrees can you move the camera up")]
        public float TopClamp = 70.0f;
        [Tooltip("How far in degrees can you move the camera down")]
        public float BottomClamp = -30.0f;
        [Tooltip("How fast the character turns to face movement direction")]
        [Range(0.0f, 0.3f)]
        public float RotationSmoothTime = 0.12f;
        [Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked")]
        public float CameraAngleOverride = 0.0f;
        [Tooltip("For locking the camera position on all axis")]
        public bool LockCameraPosition = false;
        [Tooltip("Acceleration and deceleration")]
        public float SpeedChangeRate = 10.0f;
        public float Sensibility => _sensibility;
        private bool IsCurrentDeviceMouse => _playerInput.currentControlScheme == "KeyboardMouse";

        [HideInInspector]
        public Utensil ustencil;
        private int _previousSpeed;
        private PhotonView _view;
        private UtensilHolder _utensilHolder;
        public float Stamina => _stamina;
        private float _stamina;
        private bool _frozen = true;
        public bool Frozen => _frozen;

        private void Awake()
        {
            _stamina = maxStamina;
            _sensibility = CameraSensibility;
            _animIDSpeed = Animator.StringToHash("Speed");
        }

        private void Start()
        {
            _view = GetComponent<PhotonView>();
            if (_view != null && _view.IsMine)
            {
                PlayerHUD.HUD.movement = this;
            }
        }

        private void OnDestroy()
        {
            Destroy(_tpvCam);
            if(aimCamera != null) Destroy(aimCamera.gameObject);
        }

        public void SetupUtensil()
        {
            _utensilHolder = GetComponentInChildren<UtensilHolder>();
            Transform tr = ustencil.transform;
            tr.parent = _utensilHolder.transform;
            tr.localPosition = Vector3.zero;
            tr.rotation = Quaternion.identity;
            ustencil._view = _view;
            if ((_view == null || !_view.IsMine) && ustencil is GrapplingForkUtensil gf)
            {
                gf.HideUI();
            }
        }

        void Update()
        {
            if (_view != null && !_view.IsMine) return;
            ApplyGravity();
            if (cam == null || IsFrozen()) return;

            aimCamera.gameObject.SetActive(_input.aim);
            _sensibility = _input.aim ? AimCameraSensibility : CameraSensibility;
            Move();
        }

        private void FixedUpdate()
        {
            if (_frozenTicks > 0)
            {
                _frozenTicks--;
                if (_frozenTicks == 0)
                {
                    GetComponentInChildren<Basket>().pokeAnimation.Stop(true, 
                        ParticleSystemStopBehavior.StopEmittingAndClear);
                }
            }
            else if (pokeCooldown > 0)
            {
                pokeCooldown--;
            }
        }

        private void LateUpdate()
        {
            CameraRotation();
        }

        private void ApplyGravity()
        {
            // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
            if (_verticalVelocity < _terminalVelocity)
            {
                _verticalVelocity += Gravity * Time.deltaTime;
            }
        }

        private void CameraRotation()
        {
            if (_input.look.sqrMagnitude >= _threshold && !LockCameraPosition)
            {
                float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;
                _cinemachineTargetYaw += _input.look.x * deltaTimeMultiplier * _sensibility;
                _cinemachineTargetPitch += -_input.look.y * deltaTimeMultiplier * _sensibility;
            }

            _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

            cameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
                _cinemachineTargetYaw, 0.0f);
        }

        private void Move()
        {
            float targetSpeed = BaseSpeed;
            if (_input.sprint)
            {
                if (_stamina > 0.15)
                {
                    _stamina -= Time.deltaTime;
                    targetSpeed = SprintSpeed;
                    if (_stamina < 0)
                    {
                        _stamina = 0;
                        targetSpeed = BaseSpeed;
                    }
                }
            }
            else if (_stamina < maxStamina)
            {
                _stamina += Time.deltaTime;
            }

            if (_input.move == Vector2.zero) targetSpeed = 0.0f;

            float currentHorizontalSpeed = new Vector3(controller.velocity.x, 0.0f, controller.velocity.z).magnitude;
            float speedOffset = 0.1f;
            float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;

            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset) {
                _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                    Time.deltaTime * SpeedChangeRate);
                _speed = Mathf.Round(_speed * 1000f) / 1000f; // round to 3 decimal places
            }
            else
            {
                _speed = targetSpeed;
            }

            _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * SpeedChangeRate);
            if (_animationBlend < 0.01f) _animationBlend = 0f;

            Vector3 inputDirection = new Vector3(_input.move.x, 0.0f, _input.move.y).normalized; // normalise input direction

            if (_input.move != Vector2.zero)
            {
                _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + cam.transform.eulerAngles.y;
                float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, RotationSmoothTime);
                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f); // rotate to face input direction relative to camera position
            }

            Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;
            controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) +
                            new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);

            animator.SetFloat(_animIDSpeed, _speed);
            if (_speed >= SprintSpeed && _previousSpeed != 10)
            {
                _view.RPC("RPC_SyncCharacterAnim", RpcTarget.Others, 10);
                _previousSpeed = 10;
            }
            else if (_speed >= BaseSpeed && _previousSpeed != 6)
            {
                _view.RPC("RPC_SyncCharacterAnim", RpcTarget.Others, 6);
                _previousSpeed = 6;
            }
            else if (_speed < 0.1f && _previousSpeed != 0)
            {
                _view.RPC("RPC_SyncCharacterAnim", RpcTarget.Others, 0);
                _previousSpeed = 0;
            }
        }

        private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }

        /**
         * This function is used to trigger the "Fleeing" state of nearby mikunis. Nearby mikunis
         * are all mikunis within the collider on the current GameObject.
         */
        void OnTriggerStay(Collider other)
        {
            Mikuni mikuniController =
                other.gameObject.GetComponent<Mikuni>();
            if (mikuniController != null)
            {
                // Debug.Log("Mikuni detected");
                mikuniController.PlayerNear(transform);
            }
        }
        
        [PunRPC]
        void RPC_SyncCharacterAnim(int speed)
        {
            animator.SetFloat(_animIDSpeed, speed);
        }

        [PunRPC]
        void RPC_CaptureMikuni(int viewId, bool inUtensil)
        {
            GameObject target = PhotonNetwork.GetPhotonView(viewId).gameObject;
            Mikuni capturedMikuni = target.GetComponent<Mikuni>();
            MikuniViewer viewer = inUtensil ? ustencil.viewer : 
                GetComponentInChildren<Basket>()?.gameObject.GetComponent<MikuniViewer>();
            if (capturedMikuni == null || viewer == null)
            {
                Debug.LogWarning("Received RPC_CaptureMikuni but " + (capturedMikuni == null
                    ? " the target view is not a Mikuni" : "could not find MikuniViewer"));
                return;
            }
            capturedMikuni.SetState(Mikuni.STATE_CAPTURED);
            PhotonTransformView ptv = target.GetComponent<PhotonTransformView>();
            ptv.enabled = true;
            viewer.DisplayMikuni(capturedMikuni, true);
        }
        
        [PunRPC]
        void RPC_ReleaseMikuni(int viewId, bool inUtensil)
        {
            PhotonView view = PhotonNetwork.GetPhotonView(viewId);
            GameObject target = view.gameObject;
            Mikuni capturedMikuni = target.GetComponent<Mikuni>();
            MikuniViewer viewer = inUtensil ? ustencil.viewer : 
                GetComponentInChildren<Basket>()?.gameObject.GetComponent<MikuniViewer>();
            if (capturedMikuni == null || viewer == null)
            {
                Debug.LogWarning("Received RPC_ReleaseMikuni but " + (capturedMikuni == null 
                    ? " the target view is not a Mikuni" : "could not find MikuniViewer"));
                return;
            }
            PhotonTransformView ptv = capturedMikuni.GetComponent<PhotonTransformView>();
            ptv.enabled = true; // Temporarily stop sync to avoid network glitches
            viewer.ReleaseMikuni(capturedMikuni);
        }

        [PunRPC]
        void RPC_StartCapturingSession(int playerViewID)
        {
            PhotonView vi = PhotonView.Find(playerViewID);
            PlayerController playerController = vi.gameObject.GetComponent<PlayerController>();
            if (playerController != null)
            {
                playerController.ustencil.StartCapturingSession();
            }
        }

        [PunRPC]
        void RPC_StopCapturingSession(int playerViewID)
        {
            PhotonView vi = PhotonView.Find(playerViewID);
            PlayerController playerController = vi.gameObject.GetComponent<PlayerController>();
            if (playerController != null)
            {
                playerController.ustencil.StopCapturingSession();
            }
        }

        public void Freeze()
        {
            if (!_view.IsMine) return;
            _frozen = true;
            GetComponent<Collider>().enabled = false;
        }

        public void UnFreeze()
        {
            _view = GetComponent<PhotonView>();
            if (!_view.IsMine) return;
            _frozen = false;
            GetComponent<Collider>().enabled = true;
            _tpvCam = TransformHelper.FindComponentInChildWithTag(this.gameObject, "Camera");
            _tpvCam.transform.SetParent(null);
            _tpvCam.SetActive(_view.IsMine); 
            aimCamera.gameObject.transform.SetParent(null);
        }

        public int GetPhotonViewId()
        {
            return _view.ViewID;
        }

        public void PokePlayer(int playerViewId)
        {
            if (pokeCooldown != 0) return;
            _view.RPC("RPC_PokePlayer", RpcTarget.All, playerViewId);
            pokeCooldown = POKE_COOLDOWN_MAX;
        }

        [PunRPC]
        void RPC_PokePlayer(int playerViewId)
        {
             PhotonView vi = PhotonView.Find(playerViewId);
             if (vi == null) return;
             PlayerController playerController = vi.gameObject.GetComponent<PlayerController>();
             playerController._frozenTicks = 400;
             if (playerController != null && playerController._view.IsMine)
             {
                 if (Random.Range(0, 100) > 33) // 66% dropping 1+ mikuni(s)
                 {
                     MikuniBucket bucket = playerController.GetComponentInChildren<MikuniBucket>();
                     if (Random.Range(0, 100) > 66) // 33% dropping 2 mikunis
                     {
                         bucket.ReleaseOne();
                     }
                     bucket.ReleaseOne();
                 }
             }
             playerController.GetComponentInChildren<Basket>().pokeAnimation.Play();
        }

        public bool IsFrozen()
        {
            return _frozen || _frozenTicks != 0;
        }
    }
}
