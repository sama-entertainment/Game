﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace menus
{
    public class CharacterInput : MonoBehaviour
    {
        
		public Vector2 move;
		public Vector2 look;
		public bool sprint;
		public bool releaseMikuni;
		public bool captureMikuni;
		public bool pokePlayer;
		public bool aim;
		public bool analogMovement;
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;
    
		public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}

		public void OnReleaseMikuni(InputValue value)
		{
			ReleaseMikuniInput(value.isPressed);
		}

		public void OnCaptureMikuni(InputValue value)
		{
			CaptureMikuniInput(value.isPressed);
		}

		public void OnAim(InputValue value)
		{
			AimInput(value.isPressed);
		}

		public void OnPoke(InputValue value)
		{
			PokeInput(value.isPressed);
		}

		public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void SprintInput(bool newSprintState)
		{
			sprint = newSprintState;
		}

		public void CaptureMikuniInput(bool newCaptureMikuniState)
		{
			captureMikuni = newCaptureMikuniState;
		}

		public void ReleaseMikuniInput(bool newReleaseMikuniState)
		{
			releaseMikuni = newReleaseMikuniState;
		}

		public void AimInput(bool newAimState)
		{
			aim = newAimState;
		}

		public void PokeInput(bool newPokeState)
		{
			pokePlayer = newPokeState;
		}
		
		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}
    }
}