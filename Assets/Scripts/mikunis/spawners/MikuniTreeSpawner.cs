﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

namespace mikunis.spawners
{
    public class MikuniTreeSpawner : MikuniSpawner
    {
        public GameObject mikuniPrefab;
        public List<Transform> spawnPositions;
        public int height = 10;
        public int radius = 12;
        private int checkInterval = 5000;
        private int _timer;

        private void Start()
        {
            Init(new List<GameObject> { mikuniPrefab });
            if (!PhotonNetwork.IsMasterClient)
            {
                this.enabled = false;
                return;
            }
            _timer = checkInterval;
            SpawnMikunis(spawnPositions.Count);
        }

        private void LateUpdate()
        {
            if (_timer > 0) return;

            _timer = checkInterval;
            Collider[] hitColliders = Physics.OverlapBox(
                transform.position + Vector3.up * height / 2f, new Vector3(radius, height, radius),
                Quaternion.identity, LayerMask.GetMask("Default"));
            int nearbyMikunis = 0;
            foreach (Collider obj in hitColliders)
            {
                if (obj.gameObject.GetComponent<Mikuni>() != null)
                {
                    nearbyMikunis++;
                }
            }

            if (nearbyMikunis < spawnPositions.Count)
            {
                SpawnMikunis(spawnPositions.Count-nearbyMikunis);
            }
        }

        private void FixedUpdate()
        {
            if (_timer > 0)
            {
                _timer--;
            }
        }

        private void SpawnMikunis(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int idx = Random.Range(0, spawnPositions.Count);
                GameObject gm = Spawn(mikuniPrefab, spawnPositions[idx].position, Quaternion.identity);
                gm.GetComponent<Mikuni>().SetState(Mikuni.STATE_IDLE);
            }
        }
        
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(transform.position + Vector3.up * height / 2f, new Vector3(radius, height, radius));
        }
    }
}