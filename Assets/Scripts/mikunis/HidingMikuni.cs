using System;
using System.Collections.Generic;
using UnityEngine;

namespace mikunis
{
    public class HidingMikuni : Mikuni
    {
        public float bodyStartOffset;
        public Transform body;

        public bool jumpingAnimation = true;
        
        // List of gameObject to hide when the Mikuni is hiding from the player
        public List<GameObject> bodyParts = new List<GameObject>();

        /* Jumping properties */
        private const double MaxPhase = 2 * Math.PI;
        private float jumpFrequency = 0.5f;
        private float jumpHeight = 0.6f;
        private float jumpUpdateSpeed = 0.15f; // in rad/s
        private float jumpLeaningEffect = 1.5f;
        private float jumpMaxLeaningAngle = 15; // in degrees
        
        private Vector3 _basePosition;
        private Vector3 _hiddenPosition;
        private Quaternion _baseRotation;
        private bool _hiding;
        private double _jumpPhase; // in rad

        private void Awake()
        {
            _basePosition = body.localPosition;
            _baseRotation = body.localRotation;
            _hiddenPosition = _basePosition + bodyStartOffset * Vector3.down;
            body.localPosition = _hiddenPosition;
        }

        protected override void OnAnimation()
        {
            if (!jumpingAnimation) return;
            if (State != STATE_FLEEING || CurrentSpeed <= 0.1f)
            {
                if (_jumpPhase <= 0.2f)
                {
                    body.localPosition = _hiddenPosition;
                    return;
                }
            }

            double vOffset = Math.Sin(jumpFrequency * _jumpPhase);
            _jumpPhase += jumpUpdateSpeed;
            if (_jumpPhase > MaxPhase) _jumpPhase -= MaxPhase;

            var rotation = (float) (vOffset - 0.5) * jumpLeaningEffect * jumpMaxLeaningAngle;
            var position = Vector3.up * (float) vOffset * jumpHeight;
            
            body.localPosition = _basePosition + position;
            body.localRotation = _baseRotation * Quaternion.AngleAxis(rotation, Vector3.forward);
        }

        public void ShowBodyParts()
        {
            _hiding = false;
            body.localPosition = _basePosition;
            UpdateBodyParts();
        }

        public void HideBodyParts()
        {
            _hiding = true;
            body.localPosition = _hiddenPosition;
            UpdateBodyParts();
        }

        private void UpdateBodyParts()
        {
            foreach (GameObject o in bodyParts)
            {
                Renderer objectRenderer = o.GetComponent<Renderer>();
                if (objectRenderer != null)
                {
                    objectRenderer.enabled = !_hiding;
                }
            }
        }

        public override void SetState(short state)
        {
            base.SetState(state);
            if (State == STATE_FLEEING) ShowBodyParts();
            else HideBodyParts();
        }

        private void OnDrawGizmosSelected()
        {
            if (bodyStartOffset != 0)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(_basePosition, _hiddenPosition);
            }
        }
    }
}
