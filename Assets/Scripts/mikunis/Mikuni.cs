using System;
using JetBrains.Annotations;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace mikunis
{
    public class Mikuni : MonoBehaviour, IPunObservable
    {
    
        public static readonly short STATE_IDLE = 0;
        public static readonly short STATE_FLEEING = 1;
        public static readonly short STATE_CAPTURED = 2;

        private int safeDistance = 40;
        private short _state = STATE_IDLE;
        protected float CurrentSpeed;
        private float cooldown;
        private Vector3 _previousPos;
        public uint InvincibilityTicks { get; set; }
        public short State => _state;
    
        [NotNull]
        public NavMeshAgent agent;
        public ParticleSystem spottedParticle;

        public PhotonView _view;

        protected void Start()
        {
            _view = GetComponent<PhotonView>();
            _view.ObservedComponents.Add(this);
            SetState(_state);
        }

        protected virtual void Update()
        {
            Vector3 currentPos = transform.position;
            CurrentSpeed = ((currentPos - _previousPos) / Time.deltaTime).sqrMagnitude;
            _previousPos = currentPos;
            if (cooldown > 0)
            {
                cooldown--;
            }
            else if (_view.IsMine && _state == STATE_FLEEING && CurrentSpeed <= 0.1)
            {
                if (agent.remainingDistance <= agent.stoppingDistance)
                {
                    SetState(STATE_IDLE);
                }
                else
                {
                    NavMeshHit hit;
                    if (!agent.Raycast(agent.nextPosition, out hit))
                    {
                        SetState(STATE_IDLE);
                    }
                }
            }

            OnAnimation();
        }

        private void FixedUpdate()
        {
            if (InvincibilityTicks > 0)
            {
                InvincibilityTicks--;
            }
        }

        protected virtual void OnAnimation()
        {
        }

        /**
         * This function is called when this Mikuni should switch its current state to STATE_FLEEING and
         * start fleeing from the target
         */
        public void PlayerNear(Transform player)
        {
            if (!_view.IsMine || !gameObject.activeSelf || _state == STATE_CAPTURED || cooldown != 0) return;
            if(!spottedParticle.isPlaying) spottedParticle.Play();
            SetState(STATE_FLEEING);

            Vector3 dir = (transform.position - player.transform.position).normalized;
            Vector3 newPos = transform.position + dir * safeDistance;

            int tries = 5;
            while (!agent.SetDestination(newPos) && tries > 0)
            {
                Vector2 offset = Random.insideUnitCircle * safeDistance;
                newPos = dir + new Vector3(offset.x, offset.y, 0);
                tries--;
            }
        }

        public void SetCaptured(bool captured)
        {
            SetState(captured ? STATE_CAPTURED : STATE_IDLE);
        }

        public virtual void SetState(short state)
        {
            if (state == STATE_FLEEING) cooldown = 50;
            _state = state;
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.constraints = _state == STATE_IDLE ? RigidbodyConstraints.FreezeAll : RigidbodyConstraints.None;
            agent.enabled = state == STATE_FLEEING;
            bool captured = _state == STATE_CAPTURED;
            rb.isKinematic = captured;
            rb.detectCollisions = !captured;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = _state == STATE_IDLE ? Color.magenta : (_state == STATE_FLEEING ? Color.cyan : Color.yellow);
            Gizmos.DrawWireSphere(transform.position, 1);
        }

        public bool CanBeCaptured()
        {
            return gameObject.activeSelf && _state != STATE_CAPTURED && InvincibilityTicks == 0;
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.Serialize(ref _state);
            }
            else
            {
                short previousState = _state;
                stream.Serialize(ref _state);
                if (previousState != _state)
                {
                    SetState(_state);
                }
            }
        }
    }
}
