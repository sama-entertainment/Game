using UnityEngine;

namespace mikunis
{
    public class RollingMikuni : HidingMikuni
    {

        public Transform rotatingBody;
        public Vector3 rotateDir = Vector3.right;
        public float rotSpeedX = 250f;
        private float _refSpeed;

        private void Awake()
        {
            _refSpeed = agent.speed;
            _refSpeed *= _refSpeed;
        }

        protected override void OnAnimation()
        {
            if (State != STATE_FLEEING) return;
            float speed = CurrentSpeed / _refSpeed;
            rotatingBody.Rotate(rotateDir, rotSpeedX * speed * Time.deltaTime);
        }
    }
}
