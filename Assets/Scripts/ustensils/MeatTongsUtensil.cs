﻿using System;
using UnityEngine;

namespace ustensils
{
    public class MeatTongsUtensil : Utensil
    {
        
        public GameObject tongLeft;
        public GameObject tongRight;
        
        private Vector3 activeRotation = new Vector3(-2, 23.33f, 2);

        private float angleStep = 0.3f;
        private bool _state;
        private float _angle;

        private void Start()
        {
            ActiveRot = Quaternion.Euler(activeRotation.x, activeRotation.y, activeRotation.z);
        }

        private void Update()
        {
            if (Capturing)
            {
                if (_angle > 2f && _state || _angle < -2f && !_state)
                {
                    _state = !_state;
                    _angle = _state ? -2f : 2f;
                }

                if (_state) _angle += angleStep;
                else _angle -= angleStep;

                tongLeft.transform.localRotation = Quaternion.Euler(0f, 0f, _angle);
                tongRight.transform.localRotation = Quaternion.Euler(0f, 0f, -_angle);
            }
        }
    }
}