﻿using UnityEngine;

namespace ustensils
{
    public class KatanaUtensil : Utensil
    {
        private readonly Vector3 _activeRotation = new Vector3(-92.44f, -184.5f, 16.7f);

        private void Start()
        {
            BaseRot = transform.rotation;
            ActiveRot = Quaternion.Euler(_activeRotation.x, _activeRotation.y, _activeRotation.z);
        }
    }
}