﻿using System;
using mikunis;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace ustensils
{
    public class GrapplingForkUtensil : Utensil
    {
        public const float MinimumCharge = 0.05f;
        public float momentumChargeRate = 0.006f;
        public float launchSpeed = 0.25f;
        public GameObject projectileSpawn;
        public GameObject projectilePrefab;
        public Slider chargeProgressbar;
        public GameObject crosshair;
        public LayerMask aimCollider;

        //public GameObject _hitMarker;
        private GameObject _projectile;
        private ParticleSystem _shoot;
        private float _momentumStorage;
        private Vector3 _mouseWorldPos;
        public float MomentumCharge => _momentumStorage;

        private void Start()
        {
            _shoot = GetComponentInChildren<ParticleSystem>();
            //_hitMarker.transform.SetParent(null);
            if (PhotonNetwork.PrefabPool is DefaultPool pool && !pool.ResourceCache.ContainsKey(projectilePrefab.name))
            {
                pool.ResourceCache.Add(projectilePrefab.name, projectilePrefab);
            }
        }

        public override void StartCapturingSession()
        {
            if (_capturing || _projectile != null) return;
            CaughtMikunis.Clear();
            _momentumStorage = 0;
            chargeProgressbar.gameObject.SetActive(true);
            _capturing = true;
        }

        private void Update()
        {
            chargeProgressbar.value = _momentumStorage;
            Vector2 center = new Vector2(Screen.width / 2f, Screen.height / 2f);
            Ray ray = Camera.main.ScreenPointToRay(center);
            if (Physics.Raycast(ray, out RaycastHit raycastHit, 1000f, aimCollider))
            {
                _mouseWorldPos = raycastHit.point;
                //_hitMarker.transform.position = raycastHit.point + Vector3.up * 0.01f;
            }
        }

        private void FixedUpdate()
        {
            if (_capturing && _momentumStorage < 1)
            {
                _momentumStorage += momentumChargeRate;
            }
        }

        public void HideUI()
        {
            chargeProgressbar.gameObject.SetActive(false);
            crosshair.SetActive(false);
        }

        public void ShowUI()
        {
            chargeProgressbar.gameObject.SetActive(true);
            crosshair.SetActive(true);
        }

        public override void StopCapturingSession()
        {
            if (!_capturing || _momentumStorage <= 0) return;
            if (_momentumStorage < MinimumCharge)
            {
                _momentumStorage = 0;
                _capturing = false;
                return;
            }
            Vector3 pos = projectileSpawn.transform.position;
            Vector3 dir = (_mouseWorldPos - pos).normalized;
            _projectile = PhotonNetwork.Instantiate(projectilePrefab.name, pos, Quaternion.LookRotation(dir));
            projectileSpawn.SetActive(false);
            
            ForkProjectile fork = _projectile.GetComponent<ForkProjectile>();
            fork.onHit += OnHit;
            fork.onDespawn += DespawnProjectile;

            fork.Setup(projectileSpawn, dir * launchSpeed, _momentumStorage);
            if(_shoot != null) _shoot.Play();
            
            _momentumStorage = 0;
            _capturing = false;
        }

        private void OnHit(Mikuni mikuni)
        {
            mikuni.SetCaptured(true);
            CaughtMikunis.Add(mikuni);
        }

        private void DespawnProjectile(bool hasBroke)
        {
            if (CaughtMikunis.Count > 0)
            {
                if (hasBroke) CaughtMikunis.Clear();
                else TransferMikunis();
            }
            projectileSpawn.SetActive(true);
            PhotonNetwork.Destroy(_projectile);
            _projectile = null;
        }
    }
}