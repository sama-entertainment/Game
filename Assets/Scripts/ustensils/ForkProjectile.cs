﻿using System;
using mikunis;
using Photon.Pun;
using player;
using UnityEngine;

namespace ustensils
{
    public class ForkProjectile : MonoBehaviour, IPunObservable
    {
        private float _remainingDistance;
        private float _totalDistance;
        private Vector3 _direction;
        private float _speed;
        private bool _towed;
        private GameObject launcher;
        private Mikuni capturedMikuni;

        public event Action<Mikuni> onHit;
        public event Action<bool> onDespawn;

        public LayerMask hitLayerMask;
        public int maxDistance = 30;
        private int breakDistance;
        private Vector3 launcherPos;
        public PhotonView view;
        public GameObject body;
        public GameObject heatShield;
        public LineRenderer ropeRenderer;
        public MikuniViewer viewer;

        private void Awake()
        {
            breakDistance = (maxDistance + 1) * (maxDistance + 1);
            launcherPos = transform.position;
            view.ObservedComponents.Add(this);
        }

        public void Setup(GameObject launcherObj, Vector3 direction, float momentum)
        {
            this._direction = direction;
            this._speed = direction.magnitude;
            this._totalDistance = maxDistance * momentum;
            this._remainingDistance = _totalDistance;
            this.launcher = launcherObj;
            Vector3 pos = transform.position;
            ropeRenderer.SetPosition(0, pos);
            ropeRenderer.SetPosition(1, pos);
        }
        
        private void Update()
        {
            body.transform.Rotate(Vector3.forward, 20);
            if (view != null && !view.IsMine)
            {
                ropeRenderer.SetPosition(0, launcherPos);
                ropeRenderer.SetPosition(1, transform.position);
                return;
            }

            Vector3 newPosition = transform.position;
            if (_towed)
            {
                Vector3 direction = launcher.transform.position - transform.position;
                newPosition += direction.normalized * _speed;
                float distance = direction.sqrMagnitude;
                transform.rotation = Quaternion.LookRotation(-direction);
                if (distance < 0.18)
                {
                    if (capturedMikuni != null)
                    {
                        PhotonTransformView ptv = capturedMikuni.GetComponent<PhotonTransformView>();
                        ptv.enabled = true; // Temporarily stop sync to avoid network glitches
                    }
                    viewer.Destroy();
                    onDespawn?.Invoke(false);
                } else if (distance > breakDistance)
                {
                    if (capturedMikuni != null)
                    {
                        PhotonTransformView ptv = capturedMikuni.GetComponent<PhotonTransformView>();
                        ptv.enabled = true; // Temporarily stop sync to avoid network glitches
                        view.RPC("RPC_ForkProjectile_ReleaseMikuni", RpcTarget.OthersBuffered,
                            capturedMikuni._view.ViewID);
                        capturedMikuni = null;
                    }
                    onDespawn?.Invoke(true);
                }
            }
            else
            {
                if (_remainingDistance <= 0)
                {
                    Tow();
                    view.RPC("RPC_TowForkProjectile", RpcTarget.OthersBuffered);
                }
                else
                {
                    _remainingDistance -= _speed;
                    newPosition += _direction;
                }
            }

            transform.position = newPosition;
            ropeRenderer.SetPosition(0, launcher.transform.position);
            ropeRenderer.SetPosition(1, newPosition);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!view.IsMine || _towed || capturedMikuni != null) return;
            GameObject gm = other.gameObject;
            if (gm.CompareTag("Mikuni"))
            {
                Mikuni mikuni = other.GetComponent<Mikuni>();
                if (mikuni != null && mikuni.CanBeCaptured())
                {
                    onHit?.Invoke(mikuni);
                    if (mikuni.State == Mikuni.STATE_CAPTURED)
                    {
                        viewer.Destroy();
                        PhotonTransformView ptv = mikuni.GetComponent<PhotonTransformView>();
                        ptv.enabled = false; // Temporarily stop sync to avoid network glitches
                        view.RPC("RPC_ForkProjectile_CaptureMikuni", RpcTarget.OthersBuffered, 
                            mikuni._view.ViewID);
                        viewer.DisplayMikuni(mikuni, true);
                        capturedMikuni = mikuni;
                        Tow();
                    }
                }
            }
            else if(IsInLayerMask(gm, hitLayerMask))
            {
                Tow();
                view.RPC("RPC_TowForkProjectile", RpcTarget.OthersBuffered);
            }
        }
        
        public bool IsInLayerMask(GameObject obj, LayerMask layerMask)
        {
        	return (layerMask.value & (1 << obj.layer)) > 0;
        }

        public void Tow()
        {
            if (_towed) return;
            _towed = true;
            _speed /= 2f;
            _remainingDistance = 0;
            heatShield.SetActive(false);
        }

        [PunRPC]
        void RPC_TowForkProjectile() => Tow();

        [PunRPC]
        void RPC_ForkProjectile_CaptureMikuni(int viewId)
        {
            GameObject target = PhotonNetwork.GetPhotonView(viewId).gameObject;
            Mikuni capturedMikuni = target.GetComponent<Mikuni>();
            if (capturedMikuni == null || viewer == null)
            {
                Debug.LogWarning("Received RPC_ForkProjectile_CaptureMikuni but " + (capturedMikuni == null
                    ? " the target view is not a Mikuni"
                    : "could not find MikuniViewer"));
                return;
            }

            PhotonTransformView ptv = capturedMikuni.GetComponent<PhotonTransformView>();
            ptv.enabled = true;
            viewer.DisplayMikuni(capturedMikuni, true);
        }

        [PunRPC]
        void RPC_ForkProjectile_ReleaseMikuni(int viewId)
        {
            GameObject target = PhotonNetwork.GetPhotonView(viewId).gameObject;
            Mikuni capturedMikuni = target.GetComponent<Mikuni>();
            if (capturedMikuni == null || viewer == null)
            {
                Debug.LogWarning("Received RPC_ForkProjectile_ReleaseMikuni but " + (capturedMikuni == null
                    ? " the target view is not a Mikuni"
                    : "could not find MikuniViewer"));
                return;
            }

            viewer.ReleaseMikuni(capturedMikuni);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                Vector3 pos = launcher.transform.position;
                stream.Serialize(ref pos);
            }
            else
            {
                stream.Serialize(ref launcherPos);
            }
        }
    }
}