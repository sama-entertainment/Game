using System;
using System.Collections;
using System.Collections.Generic;
using mikunis;
using Photon.Pun;
using player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ustensils
{
    [RequireComponent(typeof(Collider), typeof(Rigidbody))]
    public class Utensil : MonoBehaviour
    {
        public Sprite icon;
        public float speed;
        public uint capacity;
        [Range(0, 1)]
        public float strength;

        public float range = 0;
        public string LocalizedName;

        public event Action<List<Mikuni>> OnTransferMikunis;
        public event Action OnForbidden;

        protected Quaternion BaseRot;
        protected Quaternion ActiveRot;
        private const float lerpSpeed = 0.05f;
        private bool _dirty;
        private float _lerp;
        private bool _toActive;

        [HideInInspector]
        public PhotonView _view;
        public MikuniViewer viewer;
        private Collider _hitBox;
        protected bool _capturing;
        protected List<Mikuni> CaughtMikunis = new List<Mikuni>();
        private int fleeCheckInterval = 30;
        private int _fleeCheckTicks;

        /**
         * Returns the the number of Mikuni captured during the last capturing session
         */
        public int CapturedCount => CaughtMikunis.Count;
        public bool Capturing => _capturing;

        void Awake()
        {
            _hitBox = GetComponent<SphereCollider>();
            viewer = GetComponent<MikuniViewer>();
            _hitBox.enabled = false;
            _fleeCheckTicks = fleeCheckInterval;
            BaseRot = transform.rotation;
        }
        
        public virtual void StartCapturingSession()
        {
            if (_view != null && _view.IsMine)
            {
                _hitBox.enabled = true;
                _view.RPC("RPC_StartCapturingSession", RpcTarget.OthersBuffered, _view.ViewID);
            }
            if (!_capturing)
            {
                CaughtMikunis.Clear();
                viewer.Destroy();

                if (ActiveRot != null)
                {
                    _toActive = true;
                    _lerp = 0f;
                    _dirty = true;
                }
            }
            _capturing = true;
        }

        public virtual void StopCapturingSession()
        {
            _hitBox.enabled = false;
            _capturing = false;
            if (_view != null && _view.IsMine)
            {
                _view.RPC("RPC_StopCapturingSession", RpcTarget.OthersBuffered, _view.ViewID);
                TransferMikunis();
            }
            else
            {
                CaughtMikunis.Clear();
            }
            viewer.Destroy();
            if (ActiveRot != null)
            {
                _toActive = false;
                _lerp = 0f;
                _dirty = true;
            }
        }

        protected virtual void LateUpdate()
        {
            if (_dirty)
            {
                _lerp += lerpSpeed;
                if (_lerp >= 1f)
                {
                    _lerp = 1f;
                    if (_toActive) OnAnimation();
                    _dirty = false;
                }

                if (_toActive) transform.localRotation = Quaternion.Lerp(BaseRot, ActiveRot, _lerp);
                else transform.localRotation = Quaternion.Lerp(ActiveRot, BaseRot, _lerp);
            }

            if (!Capturing)
                return;
            if (_fleeCheckTicks > 0)
            {
                _fleeCheckTicks--;
                return;
            }

            if (_view != null && _view.IsMine && Random.Range(0f, 1f) < 0.07f * CapturedCount) // 7% chance
            {
                double proba = Random.Range(0f, 1f);
                if (proba > strength)
                {
                    //Debug.Log("A Mikuni escaped!");
                    ReleaseOneMikuni();
                }
            }
            _fleeCheckTicks = fleeCheckInterval;
        }

        protected virtual void OnAnimation()
        {
        }

        void OnTriggerEnter(Collider other)
        {
            if (_view == null || !_view.IsMine) return;
            if (CapturedCount >= capacity)
            {
                OnForbidden?.Invoke();
                return;
            }
            if (other.gameObject.CompareTag("Mikuni"))
            {
                Mikuni mikuni = other.GetComponent<Mikuni>();
                if (mikuni != null && mikuni.CanBeCaptured())
                {
                    CaptureMikuni(mikuni);
                }
            }
        }

        public void CaptureMikuni(Mikuni target)
        {
            if (CaughtMikunis.Contains(target)) return;
            CaughtMikunis.Add(target);
            viewer.DisplayMikuni(target, false);
            PhotonTransformView ptv = target.GetComponent<PhotonTransformView>();
            ptv.enabled = false; // Temporarily stop sync to avoid network glitches
            _view.RPC("RPC_CaptureMikuni", RpcTarget.OthersBuffered, target._view.ViewID, true);
        }

        protected void TransferMikunis()
        {
            CaughtMikunis.Reverse(); // oldest in front
            OnTransferMikunis?.Invoke(CaughtMikunis);
            CaughtMikunis.Clear();
        }

        public void ReleaseOneMikuni()
        {
            Mikuni mikuni = CaughtMikunis[0];
            mikuni.InvincibilityTicks = 300; // ~5s
            PhotonTransformView ptv = mikuni.GetComponent<PhotonTransformView>();
            ptv.enabled = true; // Temporarily stop sync to avoid network glitches
            _view.RPC("RPC_ReleaseMikuni", RpcTarget.AllBuffered, mikuni._view.ViewID, true);
            CaughtMikunis.RemoveAt(0);
        }
    }
}