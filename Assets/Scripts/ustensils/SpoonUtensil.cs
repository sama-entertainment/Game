﻿using System;
using UnityEngine;

namespace ustensils
{
    public class SpoonUtensil : Utensil
    {
        private readonly Vector3 _activeRotation = new Vector3(-2, 23.33f, 2);
        public GameObject particles;
        private ParticleSystem _ps;

        private void Start()
        {
            BaseRot = transform.rotation;
            _ps = particles.GetComponent<ParticleSystem>();
            ActiveRot = Quaternion.Euler(_activeRotation.x, _activeRotation.y, _activeRotation.z);
        }

        protected override void OnAnimation()
        {
            _ps.Play();
        }

        public override void StopCapturingSession()
        {
            base.StopCapturingSession();
            _ps.Stop();
        }
    }
}