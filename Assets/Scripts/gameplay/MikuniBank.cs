﻿using System;
using System.Collections;
using System.Collections.Generic;
using menus;
using mikunis;
using network.controllers;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Random = UnityEngine.Random;

namespace gameplay
{
    public class MikuniBank : MonoBehaviour
    {
        public static MikuniBank Instance;
        public GameObject redPuffAnim;
        public GameObject bluePuffAnim;

        private void Start()
        {
            Instance = this;
            if (PlayerHUD.HUD != null)
            {
                PlayerHUD.HUD.cauldronPosition = transform.position;
            }
        }

        IEnumerator DepositMikunis(PhotonPlayer player, List<Mikuni> mikunis)
        {
            List<Mikuni> cl = new List<Mikuni>(mikunis);
            Vector3 spawnCenter = Vector3.up * 6.5f;
            GameObject anim = player.teamId == 0 ? redPuffAnim : bluePuffAnim;
            GameObject puff = Instantiate(anim, transform.position + spawnCenter, anim.transform.rotation);
            int[] MikuniIds = new int[cl.Count];
            int i = 0;
            foreach (var mikuni in cl)
            {
                MikuniIds[i++] = mikuni == null ? -1 : mikuni._view.ViewID;
            }
            player._view.RPC("RPC_DropMikunis", RpcTarget.AllBuffered, MikuniIds, spawnCenter);
            foreach (var mikuni in cl)
            {
                if(mikuni == null) continue;
                player._view.RPC("RPC_NotifyMikuniDropped", RpcTarget.AllBuffered, mikuni._view.ViewID);
                yield return new WaitForSeconds(0.2f);
            }
            puff.GetComponent<ParticleSystem>().Stop();
            yield return new WaitForSeconds(1f);
            Destroy(puff);
            foreach (Mikuni mikuni in cl)
            {
                if(mikuni == null) continue;
                player.DestroyMikuni(mikuni.gameObject);
            }
        }

        public void SetupMikuni(GameObject mikuni, Vector3 pos, Quaternion rotation)
        {
            mikuni.gameObject.SetActive(true);
            Transform tr = mikuni.transform;
            tr.parent = transform.parent.transform;
            tr.localScale = Vector3.one;
            tr.localRotation = rotation;
            tr.localPosition = pos;
        }

        public bool Put(PhotonPlayer player, List<Mikuni> mikunis)
        {
            if (mikunis.Count > 12) return false;
            StartCoroutine(DepositMikunis(player, mikunis));
            player._view.RPC("RPC_PushScore", RpcTarget.MasterClient, 
                player.teamId, mikunis.Count);

            return true;
        }
    }
}