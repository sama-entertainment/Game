﻿using System;
using System.Collections;
using System.Collections.Generic;
using mikunis;
using network;
using network.controllers;
using Photon.Pun;
using sound;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace gameplay
{
    public class EndGameManager : MonoBehaviour
    {
        public int lobbySceneId = 1;
        public int mainMenuSceneId = 0;
        public GameObject redTubeSpawn;
        public GameObject redScore;
        public GameObject blueTubeSpawn;
        public GameObject blueScore;
        public GameObject victoryTitle;
        public Button replayButton;

        public List<GameObject> Mikunis;
        
        private void Start()
        {
            AudioManager.CurrentManager.StartBgMusic();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            if (PhotonNetwork.IsMasterClient)
            {
                replayButton.interactable = true;
                TextMeshProUGUI ugui = replayButton.GetComponentInChildren<TextMeshProUGUI>();
                ugui.text = "Rejouer";
                ugui.fontSize = 20f;
            }

            StartCoroutine(DisplayScores());
        }

        IEnumerator DisplayScores()
        {
            uint redCount = GameManager.Instance.scores[0];
            uint blueCount = GameManager.Instance.scores[1];
            int redCounter = 0;
            int blueCounter = 0;

            while (redCount + blueCount > 0)
            {
                if (redCount > 0)
                {
                    int idx = Random.Range(0, Mikunis.Count);
                    GameObject obj = Instantiate(Mikunis[idx], redTubeSpawn.transform.position, Quaternion.identity);
                    Setup(obj);
                    redCount--;
                    redCounter++;
                    redScore.GetComponent<TextMeshProUGUI>().text = redCounter + "";
                }

                if (blueCount > 0)
                {
                    int idx = Random.Range(0, Mikunis.Count);
                    GameObject obj = Instantiate(Mikunis[idx], blueTubeSpawn.transform.position, Quaternion.identity);
                    Setup(obj);
                    blueCount--;
                    blueCounter++;
                    blueScore.GetComponent<TextMeshProUGUI>().text = blueCounter + "";
                }
                
                yield return new WaitForSeconds(redCount + blueCount < 20 ? 0.2f : 0.1f);
            }
            yield return new WaitForSeconds(0.2f);
            string[] messages = { "Victoire de l'équipe Rouge", "Égalité", "Victoire de l'équipe Bleue" };
            victoryTitle.GetComponent<TextMeshProUGUI>().text = messages[blueCounter.CompareTo(redCounter) + 1];
        }

        private void Setup(GameObject gameObject)
        {
            gameObject.transform.localScale = Vector3.one * 0.6f;
            gameObject.GetComponent<Mikuni>().SetState(Mikuni.STATE_FLEEING);
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.detectCollisions = true;
            rb.constraints = RigidbodyConstraints.None;
            gameObject.GetComponent<NavMeshAgent>().enabled = false;
        }

        public void Replay()
        {
            if (!PhotonNetwork.IsConnectedAndReady || !PhotonNetwork.IsMasterClient)
                return;
            PhotonNetwork.LoadLevel(lobbySceneId);
        }

        public void QuitToMainMenu()
        {
            if (PhotonRoom.CurrentRoom != null)
            {
                PhotonRoom.CurrentRoom.Leave();
            }
            SceneManager.LoadSceneAsync(mainMenuSceneId);
        }
    }
}