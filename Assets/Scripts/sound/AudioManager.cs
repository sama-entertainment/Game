    using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace sound
{
    public class AudioManager : MonoBehaviour
    {

        public AudioClip[] preloadedClips;
        public static AudioManager CurrentManager;

        public AudioSource mainBgMusic;

        public AudioMixer mixer;
        private AudioSource _audioSource;

        public float MasterVolume
        {
            get
            {
                mixer.GetFloat("masterVolume", out float volume);
                return Mathf.Pow(10, volume);
            }

            set => mixer.SetFloat("masterVolume", Normalize(value));
        }

        public float MusicVolume
        {
            get
            {
                mixer.GetFloat("musicVolume", out float volume);
                return Mathf.Pow(10, volume);
            }

            set => mixer.SetFloat("musicVolume", Normalize(value));
        }

        public float SFXVolume
        {
            get
            {
                mixer.GetFloat("sfxVolume", out float volume);
                return Mathf.Pow(10, volume);
            }

            set => mixer.SetFloat("sfxVolume", Normalize(value));
        }

        private static float _masterVolume;
        private static float _musicVolume;
        private static float _sfxVolume;

        private void Start()
        {
            if (CurrentManager != null)
            {
                Destroy(gameObject);
                return;
            }
            CurrentManager = this;
            _audioSource = Instantiate(mainBgMusic);
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(_audioSource);
            StartBgMusic();
            foreach (AudioClip clip in preloadedClips)
            {
                if (clip.LoadAudioData())
                {
                    Debug.Log("Pre-loaded clip '" + clip.name + "' (" + clip.length + "s)");
                }
                else
                {
                    Debug.LogWarning("Failed to pre-load clip '" + clip.name + "'");
                }
            }
        }

        private float Normalize(float val){
            if(val <= 0){
                return -80;
            }
            return Mathf.Log10(val) * 20f;
        }

        public void StartBgMusic()
        {
            if (_audioSource.isPlaying) return;
            _audioSource.Play();
        }

        public void StopBgMusic()
        {
            _audioSource.Stop();
        }
    }
}
