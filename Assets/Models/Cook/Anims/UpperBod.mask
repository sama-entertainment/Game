%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: UpperBod
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Arm
    m_Weight: 0
  - m_Path: Belt
    m_Weight: 0
  - m_Path: Body
    m_Weight: 0
  - m_Path: Cook
    m_Weight: 0
  - m_Path: Cook/spine
    m_Weight: 0
  - m_Path: Cook/spine/pelvis.L
    m_Weight: 0
  - m_Path: Cook/spine/pelvis.L/pelvis.L_end
    m_Weight: 0
  - m_Path: Cook/spine/pelvis.R
    m_Weight: 0
  - m_Path: Cook/spine/pelvis.R/pelvis.R_end
    m_Weight: 0
  - m_Path: Cook/spine/shin.L
    m_Weight: 0
  - m_Path: Cook/spine/shin.L/shin.L.001
    m_Weight: 0
  - m_Path: Cook/spine/shin.L/shin.L.001/foot.L
    m_Weight: 0
  - m_Path: Cook/spine/shin.L/shin.L.001/foot.L/heel.02.L
    m_Weight: 0
  - m_Path: Cook/spine/shin.L/shin.L.001/foot.L/heel.02.L/heel.02.L_end
    m_Weight: 0
  - m_Path: Cook/spine/shin.L/shin.L.001/foot.L/toe.L
    m_Weight: 0
  - m_Path: Cook/spine/shin.L/shin.L.001/foot.L/toe.L/toe.L_end
    m_Weight: 0
  - m_Path: Cook/spine/shin.R
    m_Weight: 0
  - m_Path: Cook/spine/shin.R/shin.R.001
    m_Weight: 0
  - m_Path: Cook/spine/shin.R/shin.R.001/foot.R
    m_Weight: 0
  - m_Path: Cook/spine/shin.R/shin.R.001/foot.R/heel.02.R
    m_Weight: 0
  - m_Path: Cook/spine/shin.R/shin.R.001/foot.R/heel.02.R/heel.02.R_end
    m_Weight: 0
  - m_Path: Cook/spine/shin.R/shin.R.001/foot.R/toe.R
    m_Weight: 0
  - m_Path: Cook/spine/shin.R/shin.R.001/foot.R/toe.R/toe.R_end
    m_Weight: 0
  - m_Path: Cook/spine/spine.001
    m_Weight: 0
  - m_Path: Cook/spine/spine.001/spine.002
    m_Weight: 0
  - m_Path: Cook/spine/spine.001/spine.002/spine.003
    m_Weight: 0
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.L
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L_end
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.R
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R_end
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.G.001
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.G.001/mustache.G.002
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.G.001/mustache.G.002/mustache.G.003
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.G.001/mustache.G.002/mustache.G.003/mustache.G.004
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.G.001/mustache.G.002/mustache.G.003/mustache.G.004/mustache.G.004_end
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.R.001
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.R.001/mustache.R.002
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.R.001/mustache.R.002/mustache.R.003
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.R.001/mustache.R.002/mustache.R.003/mustache.R.004
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/mustache.R.001/mustache.R.002/mustache.R.003/mustache.R.004/mustache.R.004_end
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/spine.005
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006/hair
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006/hair/hair.001
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006/hair/hair.001/hair.002
    m_Weight: 1
  - m_Path: Cook/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006/hair/hair.001/hair.002/hair.002_end
    m_Weight: 1
  - m_Path: Feet
    m_Weight: 0
  - m_Path: Forearm
    m_Weight: 0
  - m_Path: Hair
    m_Weight: 0
  - m_Path: Hand
    m_Weight: 0
  - m_Path: Hat
    m_Weight: 0
  - m_Path: Head
    m_Weight: 0
  - m_Path: Head/LeftEye
    m_Weight: 0
  - m_Path: Head/RightEye
    m_Weight: 0
  - m_Path: Leg
    m_Weight: 0
  - m_Path: Mustache
    m_Weight: 0
  - m_Path: Pants
    m_Weight: 0
  - m_Path: Shoulder
    m_Weight: 0
  - m_Path: Tshirt
    m_Weight: 0
