﻿# Mikuni - Unity Project GitLab repository

This repository contains the main Unity project for Mikuni. 
To edit and build the game please use Unity Editor version 2020.3.25f1
([Unity Hub](unityhub://2020.3.25f1/9b9180224418),
[Direct Download (Win)](https://download.unity3d.com/download_unity/9b9180224418/UnityDownloadAssistant-2020.3.25f1.exe),
[Direct Download (Mac)](https://download.unity3d.com/download_unity/9b9180224418/UnityDownloadAssistant-2020.3.25f1.dmg))